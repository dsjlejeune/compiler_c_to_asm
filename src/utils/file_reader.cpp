#include<filesystem>
#include<fstream>
#include<iostream>
#include<sstream>

#include "utils/file_reader.hpp"

namespace fs = std::filesystem;

FileReader::FileReader(const fs::path& path): path(path) {
	this->stream.open(path, std::ifstream::in);

	if(!this->stream.is_open()) {
		throw FileDoesNotExistException(path);
	}
}

FileReader::~FileReader() {
	this->close();
}

std::string FileReader::read() {
	std::stringstream ss;	
	std::string buffer;

	while(std::getline(this->stream, buffer)) {
		ss << buffer << std::endl;
	}

	return ss.str();
}

void FileReader::close() {
	this->stream.close();
}
