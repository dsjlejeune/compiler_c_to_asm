class ItemSet:
    def __init__(self, items):
        """
        Item format is (rule_name, index disjunction, index conjunction)

        :param items: a list of tuples of items using the format defined above
        """
        if isinstance(items, frozenset):
            self.items = items
        else:
            tmp_set = set()
            for i in items:
                if len(i) == 3:
                    tmp_set.add(i)
                else:
                    raise ValueError('Element in item set does not conform to the format (rule_name, index disjunction, '
                                     'index conjunction):', i)
            self.items = frozenset(tmp_set)

    def __hash__(self):
        return hash(self.items)

    def __eq__(self, other):
        return self.items == other.items

