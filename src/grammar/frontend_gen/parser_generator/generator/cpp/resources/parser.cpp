/*
 * FILE GENERATED AUTOMATICALLY BY THE PARSER GENERATOR
 * DON'T MODIFY!
 * ALL MANUAL CHANGES ARE AT RISK TO BE OVERWRITTEN!
*/
#include "parser/parser.hpp"
#include "parser/parser_actions.hpp"

Parser::Parser(const Lexer& lexer): ParserBase(lexer) {
    this->buildParsingTable();
    this->init_stack($start_idx);
}

void Parser::buildParsingTable() {
$body_build_parsing_table
}