from string import Template
import os

from frontend_gen.parser_generator.generator.abstract_generator import SourceParserGenerator
from frontend_gen.parser_generator.slr_parser import Shift, Accept, Reduce


class CppSourceParserGenerator(SourceParserGenerator):

    def _generate_parsing_table(self, indentation=0):
        # Generate the goto table
        goto_lines = []
        idt = '\t'*indentation

        template_goto = Template(f'{idt}this->table_goto[std::pair<int, std::string>($state, "$rule_name")] = $goto_state;')
        for i in self.goto_table:
            to_add = template_goto.substitute(state=i[0], rule_name=i[1].name, goto_state=self.goto_table[i])
            goto_lines.append(to_add)

        goto_table_cpp = '\n'.join(goto_lines)

        action_lines = []
        template_action = Template(f'{idt}this->table_action[std::pair<int, std::string>($state, "$terminal")] = new $action;')
        for i in self.action_table:
            action = self.action_table[i]
            to_add = ''
            if isinstance(action, Shift):
                to_add = template_action.substitute(state=i[0], terminal=i[1].get_name(),
                                                    action=f'SLRShift({action.idx})')
            elif isinstance(action, Reduce):
                to_add = template_action.substitute(state=i[0], terminal=i[1].get_name(),
                                                    action=f'SLRReduce("{action.rule_name}", {action.conjunction_idx}, {action.nbr_symbols_to_pop})')
            elif isinstance(action, Accept):
                to_add = template_action.substitute(state=i[0], terminal=i[1].get_name(),
                                                    action=f'SLRAccept()')
            else:
                raise AttributeError(f'Unknown action type detected while generating the cpp parser: {action}')
            action_lines.append(to_add)

        action_table_cpp = '\n'.join(action_lines)

        return goto_table_cpp, action_table_cpp

    def generate(self):
        indentation = 1
        idt = '\t'*indentation

        goto_table, action_table = self._generate_parsing_table(indentation)
        parsing_table = f'\n{idt}// GOTO table\n{goto_table}\n\n{idt}// Action table\n{action_table}'

        resource_dir = os.path.join(os.path.split(__file__)[0], 'resources')
        lexer_cpp = 'parser.cpp'
        lexer_hpp = 'parser.hpp'
        with open(f'{resource_dir}/{lexer_cpp}') as fd:
            template_parser_cpp = Template(''.join(fd.readlines()))
            lexer_cpp_generated = template_parser_cpp.substitute(body_build_parsing_table=parsing_table,
                                                                 start_idx=self.start_idx)
        with open(f'{resource_dir}/{lexer_hpp}') as fd:
            lexer_hpp_generated = ''.join(fd.readlines())

        # Write the generated lexer to the target dir.
        main_dir = self.get_main_directory()
        include_dir = self.get_include_directory()

        with open(f'{main_dir}/{lexer_cpp}', 'w') as fd:
            fd.write(lexer_cpp_generated)
        with open(f'{include_dir}/{lexer_hpp}', 'w') as fd:
            fd.write(lexer_hpp_generated)
