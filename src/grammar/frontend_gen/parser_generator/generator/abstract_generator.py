from abc import ABC, abstractmethod


class SourceParserGenerator(ABC):
    def __init__(self, goto_table, action_table, start_idx, main_directory, include_directory=None):
        self.goto_table = goto_table
        self.action_table = action_table
        self.start_idx = start_idx
        self.main_directory = main_directory
        self.include_directory = include_directory

    def get_main_directory(self):
        return self.main_directory

    def get_include_directory(self):
        return self.include_directory

    @abstractmethod
    def generate(self):
        return NotImplemented