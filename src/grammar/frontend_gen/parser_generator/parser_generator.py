from frontend_gen.parser_generator.slr_parser import SLR


class ParserGenerator(object):
    def __init__(self, filename_bnf, entry_rule, target_generator='cpp'):
        self.filename = filename_bnf
        self.entry_rule = entry_rule
        self.generator = self._get_generator(target_generator)

    def generate(self, main_dir, include_dir=None):
        print('Generate the parser!')
        slr = SLR(self.filename, self.entry_rule)
        slr._add_augmented_entry_point()

        goto, action, start_idx = slr.generate_parsing_table()
        g = self.generator(goto_table=goto, action_table=action, start_idx=start_idx,
                           main_directory=main_dir, include_directory=include_dir)
        g.generate()

    def _get_generator(self, target):
        if target == 'cpp':
            from frontend_gen.parser_generator.generator.cpp.cpp_generator import CppSourceParserGenerator
            return CppSourceParserGenerator
        else:
            raise AttributeError('Unknown target generator')
