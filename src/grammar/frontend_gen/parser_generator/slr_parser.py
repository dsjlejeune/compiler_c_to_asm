from frontend_gen.parser_generator.bnf import *
from frontend_gen.parser_generator.item_set import ItemSet


class EndMarker(GrammarTerm):
    def __init__(self):
        self.name = '$_endmarker'

    def get_name(self):
        return self.name

    def __eq__(self, x):
        return isinstance(x, EndMarker)

    def __repr__(self):
        return self.get_name()

    def __hash__(self):
        return hash(str(self))


class Shift:
    def __init__(self, idx):
        self.idx = idx

    def __eq__(self, other):
        if isinstance(other, Shift):
            return other.idx == self.idx
        else:
            return False

    def __repr__(self):
        return f'Shift({self.idx})'


class Reduce:
    def __init__(self, rule_name, conjunction_idx, nbr_symbols_to_pop):
        self.rule_name = rule_name
        self.conjunction_idx = conjunction_idx
        self.nbr_symbols_to_pop = nbr_symbols_to_pop

    def __repr__(self):
        return f'Reduce({self.rule_name}, {self.conjunction_idx})'

    def __eq__(self, other):
        if isinstance(other, Reduce):
            return other.rule_name == self.rule_name \
                   and other.conjunction_idx == self.conjunction_idx \
                   and other.nbr_symbols_to_pop == self.nbr_symbols_to_pop
        else:
            return False


class Accept:
    def __init__(self):
        pass

    def __repr__(self):
        return 'Accept'

    def __eq__(self, other):
        if isinstance(other, Accept):
            return True
        else:
            False


class SLR:
    def __init__(self, filename_grammar, entry_rule):
        self.filename_grammar = filename_grammar
        self.entry_rule = entry_rule
        self.bnf = BNF(self.filename_grammar, start_symbol=self.entry_rule)

        self.bnf.read_rules()

        self.first_cache = {}
        self.follow_cache = {}

        self.closure_cache = {}

    def _add_augmented_entry_point(self):
        self.bnf.augment_grammar()
        self.entry_rule = self.bnf.start_symbol

    def _append_to_first(self, rule_name, x):
        if not isinstance(x, list):
            x = [x]
        if rule_name in self.first_cache:
            x_unseen = [i for i in x if i not in self.first_cache[rule_name]]
            self.first_cache[rule_name] = self.first_cache[rule_name] + x_unseen
        else:
            self.first_cache[rule_name] = x

    def first_string(self, symbols_string):
        ret = []
        if symbols_string:
            if isinstance(symbols_string[0], Terminal):
                ret.append(symbols_string[0])
            else:
                rule = self.bnf.symbol_table.get_rule(symbols_string[0].get_name())
                production_rule = rule.get_production()
                if isinstance(production_rule, ConjunctionRule):
                    conjunctions = [production_rule]
                else:
                    conjunctions = production_rule.conjunctions

                for conjunction in conjunctions:
                    cpt = 0
                    c = True
                    while cpt < len(conjunction.rules) and c:
                        if conjunction.rules[cpt:][0] != symbols_string[0]:  # Avoid self reference
                            f = self.first_string(conjunction.rules[cpt:])
                            ret += [i for i in f if i not in ret]
                            if Terminal('') in f:
                                cpt += 1
                            else:
                                c = False
                        else:
                            c = False
        return ret

    def _append_to_follow(self, rule_name, x):
        if not isinstance(x, list):
            x = [x]
        if rule_name in self.follow_cache:
            x_unseen = [i for i in x if i not in self.follow_cache[rule_name]]
            self.follow_cache[rule_name] = self.follow_cache[rule_name] + x_unseen
        else:
            self.follow_cache[rule_name] = x

    def _build_follow(self):
        for r in self.bnf.symbol_table.nonterminal:
            rule = self.bnf.symbol_table.nonterminal[r]
            rule_name = rule.get_name()
            production_rule = rule.get_production()

            if rule_name == self.entry_rule and self.entry_rule not in self.follow_cache:
                self.follow_cache[self.entry_rule] = [EndMarker()]

            if isinstance(production_rule, ConjunctionRule):
                conjunctions_rules = [production_rule]
            else:
                conjunctions_rules = production_rule.conjunctions

            for conjunction in conjunctions_rules:
                cr = conjunction.rules
                for idx in range(len(cr)):
                    if isinstance(cr[idx], NonTerminal):
                        f = self.first_string(cr[idx + 1:])
                        if f:
                            self._append_to_follow(cr[idx].get_name(), [in_follow for in_follow in f
                                                                        if in_follow != Terminal('')])
                            if Terminal('') in f:
                                self._append_to_follow(cr[idx].get_name(), rule)
                        elif cr[idx] != rule:
                            self._append_to_follow(cr[idx].get_name(), rule)

        def convert_to_terminal(key):
            terminals = []
            for candidate in self.follow_cache[key]:
                if isinstance(candidate, NonTerminal):
                    terminals += [i for i in convert_to_terminal(candidate.get_name()) if i not in terminals]
                elif candidate not in terminals:
                    terminals.append(candidate)
            return terminals

        for k in self.follow_cache:
            self.follow_cache[k] = convert_to_terminal(k)

    def follow(self, rule_name):
        if not self.follow_cache:
            self._build_follow()
        return self.follow_cache[rule_name]

    def closure(self, item_set):
        if item_set in self.closure_cache:
            return self.closure_cache[item_set]

        c = list(item_set.items)
        i = 0
        while i < len(c):
            rule = self.bnf.symbol_table.get_nonterminal(c[i][0])
            production = rule.get_production()
            if isinstance(production, DisjunctionRule):
                conjunction = production.conjunctions[c[i][1]]
            else:
                conjunction = production

            if c[i][2] < len(conjunction.rules):
                if isinstance(conjunction.rules[c[i][2]], NonTerminal):
                    candidate = conjunction.rules[c[i][2]]
                    if isinstance(candidate.get_production(), DisjunctionRule):
                        candidate_conjunctions = candidate.get_production().conjunctions
                        for n in range(len(candidate_conjunctions)):
                            new_item = (candidate.get_name(), n, 0)
                            if new_item not in c:
                                c.append(new_item)
                    else:
                        new_item = (candidate.get_name(), 0, 0)
                        if new_item not in c:
                            c.append(new_item)
            i += 1

        self.closure_cache[item_set] = frozenset(set(c))
        return self.closure_cache[item_set]

    def goto(self, item_set, symbol):
        ret = frozenset()
        for i in item_set.items:
            rule = self.bnf.symbol_table.get_rule(i[0])
            production = rule.get_production()
            if isinstance(production, DisjunctionRule):
                conjunction = production.conjunctions[i[1]]
            else:
                conjunction = production

            if i[2] < len(conjunction.rules):
                if conjunction.rules[i[2]] == symbol:
                    c = self.closure(ItemSet([(i[0], i[1], i[2] + 1)]))
                    ret = ret.union(c)

        return ret

    def get_canonical_sets(self):
        canonical_states = [self.closure(ItemSet([(self.bnf.start_symbol, 0, 0)]))]
        goto_cache = {}
        count = 0
        while count < len(canonical_states):
            items_set = canonical_states[count]
            for i in items_set:
                rule = self.bnf.symbol_table.get_rule(i[0])
                production = rule.get_production()
                if isinstance(production, DisjunctionRule):
                    conjunction = production.conjunctions[i[1]]
                else:
                    conjunction = production

                if i[2] < len(conjunction.rules):
                    item_set_inst = ItemSet(items_set)
                    term = conjunction.rules[i[2]]
                    g = self.goto(item_set_inst, term)
                    if g and g not in canonical_states:
                        canonical_states.append(g)
                    if item_set_inst not in goto_cache:
                        goto_cache[item_set_inst] = {term: g}
                    elif term not in goto_cache[item_set_inst]:
                        goto_cache[item_set_inst][term] = g

            count += 1
        return canonical_states, goto_cache

    def generate_parsing_table(self):
        """
        Builds the parsing table, i.e. the action and goto maps

        Returns:
            parsing_goto: map with keys being tuples (A,B) where A represents the canonical state index and B is a
                 nontermial object. The value is the canonical state index to go to.
            parsing_action: map with keys being tuples (A,B), where A represents the canonical state index and B is
                terminal object. The value is either the action shift, reduce or accept
            idx_start: the canonical state index of the start state
        """
        canonical_states, goto_cache = self.get_canonical_sets()
        states_idx_dict = {canonical_states[i]: i for i in range(len(canonical_states))}
        parsing_goto = {}
        parsing_action = {}
        idx_start = None

        for state in canonical_states:
            idx = states_idx_dict[state]

            for item in state:
                rule = self.bnf.symbol_table.get_rule(item[0])
                production = rule.get_production()
                if isinstance(production, DisjunctionRule):
                    conjunction = production.conjunctions[item[1]]
                else:
                    conjunction = production

                if item[2] < len(conjunction.rules):
                    symbol = conjunction.rules[item[2]]
                    itemset_goto = goto_cache[ItemSet(state)][symbol]
                    if isinstance(symbol, Terminal):
                        shift_to_idx = states_idx_dict[itemset_goto]
                        parsing_action[(idx, symbol)] = Shift(shift_to_idx)
                    elif isinstance(symbol, NonTerminal):
                        idx_goto = states_idx_dict[itemset_goto]
                        parsing_goto[(idx, symbol)] = idx_goto

                    if item[0] == self.bnf.start_symbol:
                        idx_start = idx

                elif item[0] != self.bnf.start_symbol:
                    nbr_symbols = len(conjunction.rules)
                    for symbol in self.follow(item[0]):
                        parsing_action[(idx, symbol)] = Reduce(item[0], item[1], nbr_symbols)
                elif item[0] == self.bnf.start_symbol:
                    parsing_action[(idx, EndMarker())] = Accept()

        return parsing_goto, parsing_action, idx_start
