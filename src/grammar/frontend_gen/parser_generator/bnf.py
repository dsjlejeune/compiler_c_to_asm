#!/usr/bin/python3
import re
from frontend_gen.tree.tree import *
from frontend_gen.lexer_generator.read_lex_rules import ReadLexRule
from abc import ABC, abstractmethod
import os


class GrammarTerm(ABC):

	@abstractmethod
	def get_name(self):
		pass

class Terminal(GrammarTerm):
	def __init__(self, symbol, name=None):
		self.symbol = symbol
		self.name = name

	def get_name(self):
		return self.name

	def get_symbol(self):
		return self.symbol

	def __repr__(self):
		if self.name:
			return f'terminal_{self.get_name()}("{self.get_symbol()}")'
		else:
			return f'terminal("{self.get_symbol()}")'

	def __eq__(self, x):
		if isinstance(x, Terminal):
			return x.get_name() == self.get_name() and x.get_symbol() == self.get_symbol()
		else:
			return False

	def __hash__(self):
		return hash(str(self))

class NonTerminal(GrammarTerm):
	def __init__(self, name, production=None):
		self.name = name
		self.production = production

	def get_name(self):
		return self.name
	
	def get_production(self):
		return self.production

	def set_production(self, production):
		self.production = production

	def __repr__(self):
		return f'nonTerminal("{self.get_name()}")'

	def __eq__(self, other):
		return self.name == other.name

	def __hash__(self):
		return hash(str(self))


class DisjunctionRule:
	def __init__(self, conjunctions):
		self.conjunctions = conjunctions

	def has_terminal_only(self):
		ret = True
		for i in self.conjunctions:
			if not i.has_terminal_only():
				return False
		return True

	def convert_rules_name_to_object(self):
		for i in self.conjunctions:
			i.convert_rules_name_to_object()

	def __repr__(self):
		return '|'.join([str(i) for i in self.conjunctions])


class ConjunctionRule:
	def __init__(self, statement, symbol_table, allow_use_before_definition=False):
		self.statement = statement
		self.symbol_table = symbol_table
		self.rules_name = []
		self.rules = []
		
		if self.statement:
			self.parse(self.statement)
			if not allow_use_before_definition:
				self.convert_rules_name_to_object()
		else:
			raise Exception('Attempted to build a conjunction rule but was given an empty statement')

	def parse(self, statement):
		re_statement = re.compile('(\s*<.+?>\s*|\s*\".*?\"\s*)')
		
		tokenized = [i.group().strip() for i in re_statement.finditer(statement)]

		if not tokenized:
			raise Exception('No token detected in rule')

		for i in tokenized:
			if i[0] not in ['<', '"']:
				raise Exception('Unknown symbol when trying to parse a conjunction rule')
			name = i[1:-1].strip()
			self.rules_name.append(name)
			if i[0] == '"':
				if not self.symbol_table.has_terminal(name):
					terminal = Terminal(name)
					self.symbol_table.add_terminal(terminal)

	def convert_rules_name_to_object(self):
		if not self.rules:
			rules_obj = []
			for name in self.rules_name:
				if self.symbol_table.has_nonterminal(name) or self.symbol_table.has_terminal(name):
					r = self.symbol_table.get_nonterminal(name) if self.symbol_table.has_nonterminal(name) else self.symbol_table.get_terminal(name)
					rules_obj.append(r)
				else:
					raise Exception('The rule <{}> is used but never defined'.format(name))
			self.rules = rules_obj

	def has_terminal_only(self):
		ret = True
		for i in self.rules:
			if type(i) == NonTerminal:
				return False
		return ret

	def __repr__(self):
		return ''.join([str(i) for i in self.rules])


class SymbolTable:
	def __init__(self):
		self.terminal = {}
		self.nonterminal = {}
		self.terminal_symbol = {}

	def get_terminal(self, name):
		if name in self.terminal:
			return self.terminal[name]
		else:
			return self.terminal_symbol[name]

	def get_nonterminal(self, name):
		return self.nonterminal[name]

	def get_rule(self, name):
		if name in self.terminal or name in self.terminal_symbol:
			return self.get_terminal(name)
		elif name in self.nonterminal:
			return self.get_nonterminal(name)
		else:
			raise Exception(f'Trying to get a rule that does not exist: {name}')

	def has_terminal(self, name):
		return (name in self.terminal) or (name in self.terminal_symbol)

	def has_nonterminal(self, name):
		return name in self.nonterminal

	def check_existence(self, name, terminal=False):
		object_exists = f'{"terminal" if terminal else "non-terminal"} <{name}> has already been defined.'
		if name in self.terminal:
			raise Exception(f'New definition of a terminal <{name}> while {object_exists}')
		elif name in self.nonterminal:
			raise Exception(f'New definition of a non-terminal <{name}> while {object_exists}')

	def add_terminal(self, terminal):
		if terminal.get_name():
			self.check_existence(terminal.get_name(), terminal=True)
			self.terminal[terminal.get_name()] = terminal
		self.terminal_symbol[terminal.get_symbol()] = terminal

	def add_nonterminal(self, nonterminal):
		self.check_existence(nonterminal.get_name(), terminal=False)
		self.nonterminal[nonterminal.get_name()] = nonterminal


class BNF:
	def __init__(self, filename_grammar, start_symbol=None):
		self.filename_grammar = filename_grammar

		self.symbol_table = SymbolTable()
		self.parse_tree = Tree(root_value='root')
		self.start_symbol = start_symbol
		self.is_augmented = False

	def read_rules(self):
		"""
		Read the different rules and production rules from the BNF gramnmar definition
		Note: one rule per line to make things simpler
		"""
		re_rules = re.compile('<(.+)>\s*::=\s*(.+)')
		re_import = re.compile('import\s+\"(.+)\"')
		re_comment = re.compile('#(.*)')

		with open(self.filename_grammar, 'r') as fd:
			lines = fd.readlines()
		for line in lines:
			match_group = re_rules.match(line)
			if match_group:
				non_terminal_name = match_group.group(1)
				production_string = match_group.group(2)

				new_non_terminal = NonTerminal(non_terminal_name)
				self.symbol_table.add_nonterminal(new_non_terminal)  # Need to add the new non-terminal here to handle recursive productions

				production = self._build_production_rules(production_string)

				new_non_terminal.set_production(production)

			else:
				match_include = re_import.match(line)
				if match_include:
					include_filename = match_include.group(1)
					dirname = os.path.dirname(self.filename_grammar)
					terminal_rules = ReadLexRule(f'{dirname}/{include_filename}').get_rules()  # lexer rules are relative to the grammar path
					for rule in terminal_rules:
						t = Terminal(symbol=rule[1], name=rule[0])
						self.symbol_table.add_terminal(t)
				elif line.strip() == '' or re_comment.match(line):
					pass # ignore comments and empty lines
				else:
					print(f'Warning, a line was not recognized and was ignored:\n\t{line}')

		for non_terminal_name in self.symbol_table.nonterminal:
			self.symbol_table.get_nonterminal(non_terminal_name).get_production().convert_rules_name_to_object()

	def _build_production_rules(self, production_string):
		disjunction_list = self._parse_production_rule(production_string)
		conjunctions_list = []
		
		for statement in disjunction_list:
			try:
				conjunction_rule = ConjunctionRule(statement, self.symbol_table, allow_use_before_definition=True)
				conjunctions_list.append(conjunction_rule)
			except Exception as e:
				raise Exception('An exception occurend while parsing the statement: {0}\n\n{1}'.format(statement, str(e)))
		if len(conjunctions_list) > 1:
			return DisjunctionRule(conjunctions_list)
		elif len(conjunctions_list) == 1:
			return conjunctions_list[0]
		else:
			raise Exception('It appears that no production rule was able to be found at: {}'.format(production_string))

	def _parse_production_rule(self, input_string, toggle_quote=False, toggle_bracket=False, working_buffer=None, working_list=None):
		"""
		Parses a production string. This implements a very basic push down automata covering the simple BNF production rule syntax.
		It can read terminals enclosed with double-quotes (the string can contain double-quuotes if escaped with backslash, brackets <>, vertical bar |)

		The rational of this method is that is simpler to build a push-down automata covering the features needed
		than having to build a pretty ugly regex (using lookahead)
		"""
		
		if working_buffer is None:
			working_buffer = []
		if working_list is None:
			working_list = []

		if len(input_string) == 0:
			if toggle_quote == True or toggle_bracket == True:
				raise Exception("Syntax error near: {0}{1}".format(''.join(working_list), ''.join(working_buffer)))

			working_string = ''.join(working_buffer)

			if working_string:
				working_list.append(working_string)
			
			return working_list

		c = input_string[0]
		
		if not toggle_quote and not toggle_bracket and c == '"':
			working_buffer.append(c)
			return self._parse_production_rule(input_string[1:], True, toggle_bracket, working_buffer, working_list)
		
		elif toggle_quote and c == '\\' and len(input_string)>1:
			working_buffer.append(c+input_string[1])
			return self._parse_production_rule(input_string[2:], toggle_quote, toggle_bracket, working_buffer, working_list)

		elif toggle_quote and c != '"':
			working_buffer.append(c)
			return self._parse_production_rule(input_string[1:], toggle_quote, toggle_bracket, working_buffer, working_list)
			
		elif toggle_quote and c == '"':
			working_buffer.append(c)
			return self._parse_production_rule(input_string[1:], False, toggle_bracket, working_buffer, working_list)

		elif not toggle_bracket and c == '<':
			working_buffer.append(c)
			return self._parse_production_rule(input_string[1:], toggle_quote, True, working_buffer, working_list)
		elif toggle_bracket and c != '>':
			working_buffer.append(c)
			return self._parse_production_rule(input_string[1:], toggle_quote, toggle_bracket, working_buffer, working_list)
		elif toggle_bracket and c == '>':
			working_buffer.append(c)
			return self._parse_production_rule(input_string[1:], toggle_quote, False, working_buffer, working_list)

		elif c == '|':
			working_list.append(''.join(working_buffer))
			return self._parse_production_rule(input_string[1:], toggle_quote, toggle_bracket, None, working_list)

		else:
			# Ignore space and other characters
			return self._parse_production_rule(input_string[1:], toggle_quote, toggle_bracket, working_buffer, working_list)

	def augment_grammar(self):
		if not self.start_symbol:
			raise AttributeError('The start symbol needs to be set in order to augment the grammar')

		new_rule_name = f'{self.start_symbol}_augmented_grammar_rule'
		if self.is_augmented:
			raise AttributeError('The grammar is already augmented, it cannot be augmented more than once')

		production = ConjunctionRule(f'<{self.start_symbol}>', self.symbol_table)
		new_start_rule = NonTerminal(new_rule_name, production)
		self.symbol_table.add_nonterminal(new_start_rule)

		self.start_symbol = new_rule_name
		self.is_augmented = True
