from frontend_gen.lexer_generator.regex_parser import RegexParser, RegExOp
from frontend_gen.finite_automaton.nfa import NFA
from frontend_gen.finite_automaton.states import State, SpecialTransition


class RegexesToNFA:
    def __init__(self, regexes):
        if len(regexes) == 0:
            raise AttributeError('At least one regex should be given')

        self.regexes = regexes
        self.nfa = None

    def get_nfa(self):
        if not self.nfa:
            self.nfa = self._build_main_nfa()
        return self.nfa

    def _build_main_nfa(self):
        nfas = []
        for rule_name, regex in self.regexes:
            parse_tree = RegexParser.parse(regex)
            nfa = self._build_nfa_from_tree(rule_name, f'{rule_name}_root', parse_tree.get_root())
            nfas.append(nfa)

        if len(nfas) > 1:
            main_nfa = NFA(init_state_name='main_nfa_init')
            for nfa in nfas:
                main_nfa.add_transition(main_nfa.init, nfa.init, '')
                main_nfa.accepting_states = main_nfa.accepting_states + nfa.accepting_states
            return main_nfa
        else:
            return nfas[0]

    def _build_nfa_from_tree(self, rule_name, local_name, root_node):
        """
        An implementation of the McNaughton-Yamada-Thompson algorithm
        """

        if not root_node:
            raise Exception('Empty parse tree. It might cause by an empty string regex')

        node_value = root_node.get_value()
        children_nodes = root_node.get_children()
        nfa = None

        level_name = f'{local_name}_{node_value}'

        if node_value == RegExOp.concatenation:
            nfa_left = self._build_nfa_from_tree(rule_name=rule_name, local_name=f'{level_name}_l', root_node=children_nodes[0])
            nfa_right = self._build_nfa_from_tree(rule_name=rule_name, local_name=f'{level_name}_r', root_node=children_nodes[1])

            for i in nfa_left.accepting_states:
                i.transition = nfa_right.init.transition
                i.accepting = False
            nfa_left.accepting_states = nfa_right.accepting_states
            nfa_left.possible_symbols = list(set(nfa_left.possible_symbols + nfa_right.possible_symbols))

            nfa = nfa_left

        elif node_value == RegExOp.disjunction:
            nfa = NFA(init_state_name=f'{level_name}_init')
            s_accepting = State(identifier=f'{level_name}_accepting', accepting=True, payload=rule_name)
            cpt = 0

            for i in children_nodes:
                nfa_tmp = self._build_nfa_from_tree(rule_name=rule_name, local_name=f'{level_name}_{cpt}', root_node=i)
                nfa.add_transition(nfa.init, nfa_tmp.init, '')
                for j in nfa_tmp.accepting_states:
                    j.accepting = False
                    nfa.add_transition(j, s_accepting, '')

                nfa.possible_symbols = list(set(nfa.possible_symbols + nfa_tmp.possible_symbols))
                cpt += 1

        elif node_value == RegExOp.kleenStar:
            nfa_to_kleen_star = self._build_nfa_from_tree(rule_name=rule_name, local_name=level_name, root_node=children_nodes[0])
            nfa_tmp = NFA(init_state_name=f'{level_name}_init')

            s_accepting = State(identifier=f'{level_name}_accepting', accepting=True, payload=rule_name)
            nfa_tmp.add_transition(nfa_tmp.init, nfa_to_kleen_star.init, '')
            nfa_tmp.add_transition(nfa_tmp.init, s_accepting, '')

            for i in nfa_to_kleen_star.accepting_states:
                i.accepting = False
                nfa_tmp.add_transition(i, s_accepting, '')
                nfa_tmp.add_transition(i, nfa_to_kleen_star.init, '')

            nfa_tmp.possible_symbols = nfa_to_kleen_star.possible_symbols

            nfa = nfa_tmp

        elif node_value == RegExOp.zeroOrOne:
            nfa_to_zero_or_one = self._build_nfa_from_tree(rule_name=rule_name, local_name=level_name, root_node=children_nodes[0])
            nfa_tmp = NFA(init_state_name=f'{level_name}_init')

            s_accepting = State(identifier=f'{level_name}_accepting', accepting=True, payload=rule_name)
            nfa_tmp.add_transition(nfa_tmp.init, nfa_to_zero_or_one.init, '')
            nfa_tmp.add_transition(nfa_tmp.init, s_accepting, '')

            for i in nfa_to_zero_or_one.accepting_states:
                i.accepting = False
                nfa_tmp.add_transition(i, s_accepting, '')

            nfa_tmp.possible_symbols = nfa_to_zero_or_one.possible_symbols

            nfa = nfa_tmp

        elif node_value == RegExOp.oneOrMore:
            nfa_to_one_or_more = self._build_nfa_from_tree(rule_name=rule_name, local_name=level_name, root_node=children_nodes[0])
            nfa_tmp = NFA(init_state_name=f'{level_name}_init')

            s_accepting = State(identifier=f'{level_name}_accepting', accepting=True, payload=rule_name)
            nfa_tmp.add_transition(nfa_tmp.init, nfa_to_one_or_more.init, '')

            for i in nfa_to_one_or_more.accepting_states:
                i.accepting = False
                nfa_tmp.add_transition(i, nfa_to_one_or_more.init, '')
                nfa_tmp.add_transition(i, s_accepting, '')

            nfa_tmp.possible_symbols = nfa_to_one_or_more.possible_symbols

            nfa = nfa_tmp

        else:
            if node_value == RegExOp.anyChar:
                node_value = SpecialTransition.ANY
            nfa = NFA(init_state_name=level_name, is_init_accepting=False)
            s_accepting = State(identifier=f'{level_name}_accepting', accepting=True, payload=rule_name)
            nfa.add_transition(nfa.init, s_accepting, node_value)

        return nfa

