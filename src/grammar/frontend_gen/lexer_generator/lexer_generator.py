from frontend_gen.lexer_generator.read_lex_rules import ReadLexRule
from frontend_gen.lexer_generator.regex_to_nfa import RegexesToNFA


class LexerGenerator(object):
    def __init__(self, filename_regex, target_generator='cpp'):
        self.filename = filename_regex
        self.regexes = ReadLexRule(self.filename).get_rules()
        self.nfa = self._build_nfa(self.regexes)
        self.generator = self._get_generator(target_generator)

    def generate(self, main_dir, include_dir=None):
        print('Generate the lexer!')
        states, transition_matrix = self._build_transition_matrix()
        g = self.generator(states=states, transition_matrix=transition_matrix, init_state_id=self.nfa.init.identifier,
                           main_directory=main_dir, include_directory=include_dir)
        g.generate()

    def _build_transition_matrix(self):

        transition_matrix_list = []
        stack_states = [self.nfa.init]
        visited_states = [self.nfa.init]

        while stack_states:
            s = stack_states.pop()
            for (on_transition, to_states) in s.transition.items():
                for state in to_states:
                    if state not in visited_states:
                        stack_states.append(state)
                        visited_states.append(state)
                    t = [s, state, on_transition]
                    transition_matrix_list.append(t)
        return visited_states, transition_matrix_list

    def _build_nfa(self, regexes):
        r = RegexesToNFA(regexes)
        return r.get_nfa()

    def _get_generator(self, target):
        if target == 'cpp':
            from frontend_gen.lexer_generator.generator.cpp.cpp_generator import CppSourceLexerGenerator
            return CppSourceLexerGenerator
        else:
            raise AttributeError('Unknow target generator')
