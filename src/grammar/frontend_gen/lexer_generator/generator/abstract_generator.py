from abc import ABC, abstractmethod


class SourceLexerGenerator(ABC):
    def __init__(self, states, transition_matrix, init_state_id, main_directory, include_directory=None):
        self.states = states
        self.transition_matrix = transition_matrix
        self.init_state_id = init_state_id
        self.main_directory = main_directory
        self.include_directory = include_directory

    def get_main_directory(self):
        return self.main_directory

    def get_include_directory(self):
        return self.include_directory

    @abstractmethod
    def generate(self):
        return NotImplemented

