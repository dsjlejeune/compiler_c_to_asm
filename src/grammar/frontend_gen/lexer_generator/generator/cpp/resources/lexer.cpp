/*
 * FILE GENERATED AUTOMATICALLY BY THE LEXER GENERATOR
 * DON'T MODIFY!
 * ALL MANUAL CHANGES ARE AT RISK TO BE OVERWRITTEN!
*/
#include "lexer/lexer_base.hpp"
#include "finite_automaton/nfa.hpp"
#include "lexer/lexer.hpp"

Lexer::Lexer(const fs::path& filename): LexerBase(filename) {
    this->buildNFA();
}

void Lexer::buildNFA() {
$body_build_nfa
}