# import frontend_gen.lexer_generator.generator.cpp.resources as resources
import os
from string import Template

from frontend_gen.finite_automaton.states import SpecialTransition
from frontend_gen.lexer_generator.generator.abstract_generator import SourceLexerGenerator


class CppSourceLexerGenerator(SourceLexerGenerator):

    def _generate_rule_define(self, states):
        template = Template('#define LEXEME_$lexeme "$id"')
        defines = []
        rules = []

        for s in states:
            if s.payload and s.payload.strip() not in rules:
                id = s.payload.strip()
                defines.append(template.substitute(lexeme=id.upper(), id=id))
                rules.append(id)
        return defines

    def _format_char(self, char):
        if char == '"':
            return '\\"'
        elif char == '\t':
            return '\\t'
        elif char == '\n':
            return '\\n'
        elif char == SpecialTransition.ANY:
            return 'ANY'
        else:
            return char

    def _generate_nfa(self, identation=0):
        visited_state = {}
        id_cpt = 0
        level_indentation = '\t'.join(['' for i in range(identation+1)])
        template_state_def = Template(f'{level_indentation}auto $var_name = FAState<std::string, std::string>("$id", "$rule_name", "", $accepting);')
        template_transition = Template(f'{level_indentation}this->nfa->addTransition($var_from_state, $var_to_state, "$on_char");')

        nfa_mem_allocation = Template(f'{level_indentation}this->nfa = new NFA<std::string>($var_state_init);\n\n')
        state_def = []
        transition_def = []

        for [from_state, to_state, on_char] in self.transition_matrix:
            on_char_formatted = self._format_char(on_char)
            if from_state.identifier not in visited_state:
                id_cpt += 1
                visited_state[from_state.identifier] = id_cpt
                state_def.append(template_state_def.substitute(
                    var_name=f's_{id_cpt}'
                    , id=fr'{id_cpt}'
                    , rule_name=from_state.payload
                    , accepting='true' if from_state.accepting else 'false'
                ))
            if to_state.identifier not in visited_state:
                id_cpt += 1
                visited_state[to_state.identifier] = id_cpt
                state_def.append(template_state_def.substitute(
                    var_name=f's_{id_cpt}'
                    , id=fr'{id_cpt}'
                    , rule_name=to_state.payload
                    , accepting='true' if to_state.accepting else 'false'
                ))

            transition_def.append(
                template_transition.substitute(
                    var_from_state=f's_{visited_state[from_state.identifier]}'
                    , var_to_state=f's_{visited_state[to_state.identifier]}'
                    , on_char=on_char_formatted
                )
            )
        nfa_new = nfa_mem_allocation.substitute(var_state_init=f"s_{visited_state[self.init_state_id]}")
        return '\n'.join(state_def) + '\n\n' + nfa_new + '\n'.join(transition_def)

    def generate(self):
        nfa_cpp = self._generate_nfa(identation=1)
        resource_dir = os.path.join(os.path.split(__file__)[0], 'resources')
        lexer_cpp = 'lexer.cpp'
        lexer_hpp = 'lexer.hpp'
        with open(f'{resource_dir}/{lexer_cpp}') as fd:
            template_lexer_cpp = Template(''.join(fd.readlines()))
            lexer_cpp_generated = template_lexer_cpp.substitute(body_build_nfa=nfa_cpp)
        with open(f'{resource_dir}/{lexer_hpp}') as fd:
            lexer_hpp_generated = ''.join(fd.readlines())

        # Write the generated lexer to the target dir.
        main_dir = self.get_main_directory()
        include_dir = self.get_include_directory()

        with open(f'{main_dir}/{lexer_cpp}', 'w') as fd:
            fd.write(lexer_cpp_generated)
        with open(f'{include_dir}/{lexer_hpp}', 'w') as fd:
            fd.write(lexer_hpp_generated)

    def get_header_message(self):
        return NotImplemented