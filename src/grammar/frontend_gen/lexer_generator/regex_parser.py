from enum import Enum
from frontend_gen.tree.tree import TreeNode, Tree


class RegExOp(Enum):
    kleenStar = '*'
    disjunction = '|'
    concatenation = 'o'
    oneOrMore = '+'
    zeroOrOne = '?'
    anyChar = '.'


class RegexParser:
    def __init__(self):
        pass

    @staticmethod
    def _get_char(c):
        if c == '.':
            return RegExOp.anyChar
        else:
            return c

    @staticmethod
    def _parse_char(regex):
        supported_special_chars = ['*', '|', '+', '?', '.', '[', ']', '(', ')', '-']
        if len(regex) >= 2 and regex[0] == '\\':
            if regex[1] in supported_special_chars:
                return regex[1], 1
            else:
                return regex[0:2].encode('utf-8').decode('unicode-escape'), 1  # UTF8 should be ok as we don't expect other type of char (e.g. one from iso-8859-1 charset) here.
        else:
            return RegexParser._get_char(regex[0]), 0

    @staticmethod
    def _update_lookahead(sentinel_idx, length_regex):
        return  sentinel_idx+1 if sentinel_idx + 1 < length_regex else None

    @staticmethod
    def _parse_bracket_expression(regex):
        root = None
        sentinel_idx = 0
        length_regex = len(regex)

        def sign(x):
            return 1 if x >= 0 else -1

        while sentinel_idx < len(regex):
            current_char, idx = RegexParser._parse_char(regex[sentinel_idx:])
            sentinel_idx += idx
            lookahead = RegexParser._update_lookahead(sentinel_idx, length_regex)

            if current_char == ']':
                break

            if not root:
                root = Tree(RegExOp.disjunction)

            # Range
            if lookahead and regex[lookahead] == '-':
                if sentinel_idx+2 < len(regex) and regex[sentinel_idx+2] not in ['*', '|', '+', '?', '-', ']']:
                    new_char_in_range, idx = RegexParser._parse_char(regex[sentinel_idx+2])

                    if len(new_char_in_range) > 1 or len(current_char) > 1:
                        raise NotImplementedError('Escaped character are not yet supported in range')

                    s = sign(ord(new_char_in_range) - ord(current_char))
                    r = [TreeNode(chr(i)) for i in range(ord(current_char), ord(new_char_in_range)+s, s)]
                    root.get_root().add_children(r)
                    sentinel_idx += 2+idx
                else:
                    raise Exception('Syntax Error: invalid character in a range expression')
            else:
                root.get_root().add_child(TreeNode(current_char))

            if lookahead and regex[lookahead] == '|':
                sentinel_idx += 2
            else:
                sentinel_idx += 1

        if regex[sentinel_idx] != ']':
            raise Exception('Syntax error: expression does not have matching closing bracket(s)')

        return root, sentinel_idx

    @staticmethod
    def _parse_expression(regex, within_parenthesis=False):
        root = None
        sentinel_idx = 0
        length_regex = len(regex)

        regex_operator_list = ['*', '+', '?']

        if len(regex) == 0:
            return None, -1
        elif len(regex) == 1:
            if regex in regex_operator_list or regex == '|':
                Exception('Syntax Error: operator without arguments')
            else:
                return Tree(regex), 1

        while sentinel_idx < len(regex):
            subtree_group = None
            if regex[sentinel_idx] == '(':
                subtree_group, idx = RegexParser._parse_expression(regex[sentinel_idx+1:], within_parenthesis=True)

                sentinel_idx += (idx+1) # Now pointing, hopefully, on the corresponding closing parenthesis

                if sentinel_idx >= length_regex:
                    raise Exception('Syntax Error: found an opening parenthesis without a closing one')

            elif regex[sentinel_idx] == ')':
                if within_parenthesis:
                    return root, sentinel_idx # returns subtree
                else:
                    raise Exception('Syntax Error: found closing parenthesis without an opening on')

            if regex[sentinel_idx] == '[':
                subtree_group, idx = RegexParser._parse_bracket_expression(regex[sentinel_idx+1:])
                sentinel_idx += (idx+1) # Now pointing, hopefully, on the corresponding closing parenthesis

            current_char, idx = RegexParser._parse_char(regex[sentinel_idx:])
            sentinel_idx += idx
            lookahead = RegexParser._update_lookahead(sentinel_idx, length_regex)

            new_tree_node = subtree_group.get_root() if subtree_group else TreeNode(current_char)
            new_operation = RegExOp.disjunction if (lookahead and regex[lookahead] == '|') else RegExOp.concatenation

            subtree = Tree(new_operation)

            if lookahead and regex[lookahead] == '|':
                right_tree, idx = RegexParser._parse_expression(regex[lookahead+1:], within_parenthesis=within_parenthesis)

                if not root:
                    left_tree = Tree(new_tree_node.value)
                else:
                    left_tree = Tree(RegExOp.concatenation)
                    left_tree.get_root().add_children([root.get_root(), new_tree_node])

                # Flatten the disjunctions of disjunctions recursively
                if right_tree.get_root().value == RegExOp.disjunction:
                    subtree.get_root().add_children([left_tree.get_root()] + right_tree.get_root().children)
                else:
                    subtree.get_root().add_children([left_tree.get_root(), right_tree.get_root()])

                root = subtree

                sentinel_idx += (idx+1)

            elif not lookahead or regex[lookahead] not in regex_operator_list:
                if not root:
                    subtree.root = new_tree_node
                else:
                    subtree.get_root().add_child(root.get_root())
                    subtree.get_root().add_child(new_tree_node)

                root = subtree

            else:
                if regex[lookahead] == '*':
                    op = RegExOp.kleenStar
                elif regex[lookahead] == '+':
                    op = RegExOp.oneOrMore
                elif regex[lookahead] == '?':
                    op = RegExOp.zeroOrOne
                else:
                    raise Exception(f'Syntax Error: not a recognized regex operation: {regex[lookahead]}')

                if not root:
                        subtree.root = TreeNode(op).add_child(new_tree_node)
                else:
                    subtree.get_root().add_children([root.get_root(), TreeNode(op).add_child(new_tree_node)])

                root = subtree
                sentinel_idx = lookahead

            sentinel_idx += 1

        return root, sentinel_idx

    @staticmethod
    def parse(regex):
        parse_tree, _ = RegexParser._parse_expression(regex)
        return parse_tree