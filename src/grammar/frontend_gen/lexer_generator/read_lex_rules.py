import re


class ReadLexRule(object):
    def __init__(self, filename):
        self.filename = filename
        self.rules = []

        with open(self.filename, 'r') as fd:
            rows = fd.readlines()
            self.rules = self._parse_rules(rows)

    def get_rules(self):
        return self.rules

    def _parse_rules(self, rows):
        rules = []
        rule_re = re.compile(r'<(.+)> ::= (.+)')
        for i in rows:
            if i:  # Ignore empty lines
                m = rule_re.match(i)
                if m:
                    rule_name = m.group(1)
                    regex = m.group(2)
                    rules.append([rule_name, regex])
                else:
                    pass
        return rules