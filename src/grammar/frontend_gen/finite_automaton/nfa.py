from frontend_gen.finite_automaton.dfa import DFA
from frontend_gen.finite_automaton.states import State, SetOfStates


class NFA(object):
    def __init__(self, init_state_name='init', is_init_accepting=False):
        self.init = State(init_state_name, is_init_accepting)
        self.possible_symbols = [] # aka alphabet
        self.accepting_states = []

    def add_transition(self, from_state, to_state, on_transition):
        from_state.add_transition(to_state, on_transition)
        if on_transition not in self.possible_symbols:
            self.possible_symbols.append(on_transition)
        if to_state.accepting and to_state not in self.accepting_states:
            self.accepting_states.append(to_state)

    def match(self, input_string):
        m, _ = self.match_with_payload(input_string)
        return m

    def match_with_payload(self, input_string):
        closure_s = self.epsilon_closure([self.init])
        for c in input_string:
            closure_s = self.epsilon_closure(self.move(closure_s, c))

        for s in closure_s:
            if s.accepting:
                return True, s.get_payload()

        return False, None

    def epsilon_closure(self, from_states):
        if type(from_states) == list:
            closure = [i for i in from_states]
            stack = [i for i in from_states]
        else:
            closure = [from_states]
            stack = [from_states]

        while stack:
            state = stack.pop()
            states_by_epsilon = state.get_epsilon_transition_states()
            for s in states_by_epsilon:
                if not s in closure:
                    closure.append(s)
                    stack.append(s)
        return closure

    def move(self, from_states, char):
        ret = []

        for s in from_states:
            ret += s.get_non_epsilon_transition_states(char)

        return list(set(ret)) # removing duplicates

    def to_dfa(self):
        dfa_states = {}
        dfa_init_state = SetOfStates(self.epsilon_closure([self.init]))
        dfa_states_unmarked = [dfa_init_state]
        dfa_states_marked = []

        dfa = DFA(dfa_init_state)

        while dfa_states_unmarked:
            s = dfa_states_unmarked.pop()

            if type(s) == SetOfStates:
                current_state = s
            else:
                current_state = SetOfStates(s)

            if current_state not in dfa_states:
                dfa_states[current_state] = current_state
            else:
                current_state = dfa_states[current_state]

            dfa_states_marked.append(current_state)
            for symbol in self.possible_symbols:
                if symbol != '':
                    epsilon_closure = self.epsilon_closure(self.move(current_state.states, symbol))
                    next_states = SetOfStates(epsilon_closure)

                    if next_states not in dfa_states:
                        dfa_states[next_states] = next_states
                    else:
                        next_states = dfa_states[next_states]

                    if (next_states not in dfa_states_unmarked) and (next_states not in dfa_states_marked):
                        dfa_states_unmarked.append(next_states)

                    dfa.add_transition(current_state, next_states, symbol)

        return dfa
