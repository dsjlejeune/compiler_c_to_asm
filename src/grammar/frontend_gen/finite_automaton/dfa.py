from frontend_gen.finite_automaton.states import State, SetOfStates


class DFA(object):
	def __init__(self, init_state):
		self.init = init_state

	def add_transition(self, from_state, to_state, on_transition):
		from_state.add_transition(to_state, on_transition)

	def match(self, input_string):
		current_state = self.init
		for c in input_string:
			if c in current_state.transition:
				current_state = current_state.transition[c][0]
			else:
				return False

		if current_state.accepting:			
			return True
		else:
			return False

