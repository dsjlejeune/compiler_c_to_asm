from enum import Enum


class SpecialTransition(Enum):
    ANY = 'DOTANY'


class State(object):
    def __init__(self, identifier, accepting=False, payload=None):
        self.identifier = identifier
        self.accepting = accepting
        self.payload = payload

        self.transition = {}

    def add_transition(self, to_state, on_transition):
        if on_transition in self.transition:
            self.transition[on_transition] += [to_state]
        else:
            self.transition[on_transition] = [to_state]

    def get_non_epsilon_transition_states(self, transition):
        if transition in self.transition:
            return self.transition[transition]
        elif SpecialTransition.ANY in self.transition:
            return self.transition[SpecialTransition.ANY]
        else:
            return []

    def get_epsilon_transition_states(self):
        if '' in self.transition:
            return self.transition['']
        else:
            return []

    def get_payload(self):
        return self.payload

    def __hash__(self):
        return hash(self.identifier)

    def __repr__(self):
        return str(self.identifier)

    def __eq__(self, other):
        if other:
            return self.identifier == other.identifier
        else:
            return False


class SetOfStates(object):
    def __init__(self, states=None):
        if not states:
            states = []

        self.accepting = False

        self.states = None
        self.set_states(states)

        self.transition = {}

    def add_state(self, state):
        if state not in self.states:
            self.states.append(state)
            if self.accepting == False and state.accepting:
                self.accepting = True

    def set_states(self, states):
        self.states = states
        for s in self.states:
            if s.accepting:
                self.accepting = True
                break

    def add_transition(self, to_state, on_transition):
        self.transition[on_transition] = to_state # SetOfState is used only in DFA, so only a state is stored in the transition hashmap, not a list

    def __hash__(self):
        self_ids = sorted([str(s.identifier) for s in self.states])
        return hash(''.join(self_ids))

    def __eq__(self, other):
        if len(self.states) != len(other.states):
            return False

        self_ids = sorted([s.identifier for s in self.states])
        other_ids = sorted([s.identifier for s in other.states])
        for x,y in zip(self_ids, other_ids):
            if x != y:
                return False
        return True

    def __str__(self):
        ids = sorted([s.identifier for s in self.states])
        s = ','.join(ids)
        return f'[{s}]'

    def __repr__(self):
        return self.__str__()
