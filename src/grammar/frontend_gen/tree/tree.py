class TreeNode:
    def __init__(self, value, children=None, metadata=None):
        self.children = []
        self.value = value

        self.metadata = None
        self.set_metadata(metadata)

        if children:
            self.children = children

    def add_child(self, node):
        self.children.append(node)
        return self

    def add_children(self, children):
        self.children += children
        return self

    def get_children(self):
        return self.children

    def get_value(self):
        return self.value

    def get_metadata(self):
        return self.metadata

    def set_metadata(self, metadata):
        self.metadata = metadata

    def __repr__(self):
        return self.value.__repr__()

    def __eq__(self, node):
        if isinstance(node, self.__class__) and self.value == node.value and len(self.children) == len(node.get_children())\
                and sum([self.children[i] == node.get_children()[i] for i in range(len(self.children))]) == len(self.children):
            return True
        else:
            return False

class Tree:
    def __init__(self, root_value):
        self.root = TreeNode(value=root_value)

    def add_child_to_node(self, node, child_node):
        node.add_child(child_node)

    def get_children(self, node):
        return node.get_children()

    def get_root(self):
        return self.root

    def dfs(self, node):
        if not node:
            return []

        ret = [node] + [self.dfs(child) for child in node.get_children()]

        return ret

