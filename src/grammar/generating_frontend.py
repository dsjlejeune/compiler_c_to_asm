#!/usr/bin/python3

from frontend_gen.lexer_generator.lexer_generator import LexerGenerator
from frontend_gen.parser_generator.parser_generator import ParserGenerator
import pathlib

if __name__ == '__main__':
	lexer = LexerGenerator('lexer.reg')
	lexer.generate('../lexer', '../../include/lexer')

	# The parser needs the grammar path to be absolute to be able to find the included lexer file
	parser = ParserGenerator(pathlib.Path('c_grammar.bnf').resolve(), entry_rule='E')
	parser.generate('../parser', '../../include/parser')
