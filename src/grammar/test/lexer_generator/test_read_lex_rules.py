import unittest
from unittest.mock import patch, mock_open

from frontend_gen.lexer_generator.read_lex_rules import *


class TestReadLexRules(unittest.TestCase):

    def test_parsing_one_rule(self):
        # Given
        test_rule_name = 'test_name'
        test_regex = '(name)*'
        test_rule = f'<{test_rule_name}> ::= {test_regex}'

        # When
        with patch('builtins.open', mock_open(read_data=test_rule)):
            subject = ReadLexRule(filename='test_lex_rule.reg')

        # Then
        self.assertCountEqual(subject.get_rules(), [[test_rule_name, test_regex]])

    def test_parsing_multiple_rules(self):
        # Given
        test_rule_names = ['test_name_1', 'test_name_2', 'test-name-3']
        test_regexes = ['a+', 'b*', 'c']
        expected = [[i, j] for i, j in zip(test_rule_names, test_regexes)]
        test_rules = '\n'.join([f'<{i}> ::= {j}' for i, j in expected])

        # When
        with patch('builtins.open', mock_open(read_data=test_rules)):
            subject = ReadLexRule(filename='test_lex_rule.reg')

        # Then
        self.assertCountEqual(subject.get_rules(), expected)

    def test_parsing_empty_rows(self):
        # Given
        test_rule_names = ['test_name_1', 'test_name_2', 'test-name-3']
        test_regexes = ['a+', 'b*', 'c']
        expected = [[i, j] for i, j in zip(test_rule_names, test_regexes)]
        test_rules = '\n\n'.join([f'<{i}> ::= {j}' for i, j in expected])

        # When
        with patch('builtins.open', mock_open(read_data=test_rules)):
            subject = ReadLexRule(filename='test_lex_rule.reg')

        # Then
        self.assertCountEqual(subject.get_rules(), expected)