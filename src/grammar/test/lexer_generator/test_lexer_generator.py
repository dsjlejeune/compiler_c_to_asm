import unittest
from unittest.mock import patch, mock_open

from frontend_gen.finite_automaton.nfa import *
from frontend_gen.lexer_generator.generator.cpp.cpp_generator import *
from frontend_gen.lexer_generator.lexer_generator import *


class TestLexerGenerator(unittest.TestCase):

    def test_get_cpp_generator(self):
        # Given
        test_rule_name = 'test_name'
        test_regex = '(name)*'
        mokup_rules = f'<rule_1> ::= 1\n<rule_2> ::= 2'

        # When
        with patch('builtins.open', mock_open(read_data=mokup_rules)):
            subject = LexerGenerator('mokup', target_generator='cpp')

        # Then
        self.assertEqual(subject.generator.__name__, CppSourceLexerGenerator.__name__)

    def test_get_nfa_transition_matrix(self):
        # Given
        mokup_rules = f'<rule_1> ::= 1\n<rule_2> ::= 2'

        s2 = State('2', False)
        s3 = State('3', False)
        s4 = State('4', True)
        s5 = State('5', True)

        nfa = NFA()

        nfa.add_transition(nfa.init, s2, '')

        nfa.add_transition(nfa.init, s3, '')
        nfa.add_transition(s2, s4, '1')
        nfa.add_transition(s3, s5, '2')

        expected_states = [nfa.init, s2, s3, s4, s5]
        expected_transition_table = [[nfa.init, s2, ''], [nfa.init, s3, ''], [s2, s4, '1'], [s3, s5, '2']]

        # When
        with patch('builtins.open', mock_open(read_data=mokup_rules)), \
             patch.object(LexerGenerator, '_build_nfa', return_value=nfa):
            lexer_generator = LexerGenerator('mokup', target_generator='cpp')
            states, transition_table = lexer_generator._build_transition_matrix()

        # Then
        self.assertEqual(len(states), 5)
        self.assertCountEqual(states, expected_states)

        self.assertEqual(len(transition_table), 4)
        self.assertCountEqual(transition_table, expected_transition_table)

        self.assertCountEqual([i[1] for i in transition_table if i[1].accepting], [s4, s5])
        self.assertCountEqual([i[0] for i in transition_table if i[0].accepting], [])
        self.assertCountEqual([i[1] for i in transition_table if not i[1].accepting], [s2, s3])
        self.assertCountEqual([i[0] for i in transition_table if not i[0].accepting], [nfa.init, nfa.init, s2, s3])