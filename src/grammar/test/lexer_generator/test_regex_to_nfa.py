import unittest
from frontend_gen.finite_automaton.nfa import NFA
from frontend_gen.finite_automaton.states import State
from frontend_gen.lexer_generator.regex_to_nfa import RegexesToNFA

class TestRegexToNFA(unittest.TestCase):

    def test_one_rule_empty(self):
        # Given
        rules_expresions = ['enpty_rule', r'a']
        rnfa = RegexesToNFA(rules_expresions)

        # When
        # Then
        with self.assertRaisesRegex(Exception, r'.*'):
            rnfa.get_nfa()

    def test_one_rule_one_char(self):
        # Given
        rules_name = ['some_rule']
        rules_expresions = [[rules_name[0], r'a']]
        s_accepting = State(f'{rules_name[0]}_root_a_accepting', rules_name[0])
        expected = NFA(init_state_name=f'{rules_name[0]}_init')
        expected.add_transition(expected.init, s_accepting, 'a')

        rnfa = RegexesToNFA(rules_expresions)

        # When
        subject = rnfa.get_nfa()

        # Then
        self.assertEqual(type(subject), NFA)
        self.assertEqual(subject.init.transition, {'a': [s_accepting]})
        self.assertTrue(len(subject.accepting_states), 1)

        self.assertEqual(subject.match_with_payload('a'), (True, rules_name[0]))
        self.assertFalse(subject.match('b'))
        self.assertFalse(subject.match(''))
        self.assertFalse(subject.match('aa'))

    def test_multiple_rules_one_char(self):
        # Given
        rules_expresions = [['rule_a', r'a'], ['rule_b', r'b'], ['rule_c', r'c']]
        rnfa = RegexesToNFA(rules_expresions)

        # When
        subject = rnfa.get_nfa()

        # Then
        self.assertEqual((True, 'rule_a'), subject.match_with_payload('a'))
        self.assertEqual((True, 'rule_b'), subject.match_with_payload('b'))
        self.assertEqual((True, 'rule_c'), subject.match_with_payload('c'))

    def test_one_rule_concatenation_only(self):
        # Given
        rules_expresions = [['rule_abc', r'abc']]
        rnfa = RegexesToNFA(rules_expresions)

        # When
        subject = rnfa.get_nfa()

        # Then
        self.assertTrue(subject.match('abc'))
        self.assertFalse(subject.match('abca'))
        self.assertFalse(subject.match(''))
        self.assertFalse(subject.match('a'))
        self.assertFalse(subject.match('ab'))
        self.assertFalse(subject.match('c'))

    def test_one_rule_concatenation_with_group(self):
        # Given
        rules_expresions = [['rule_abc', r'a(bc)d']]
        rnfa = RegexesToNFA(rules_expresions)

        # When
        subject = rnfa.get_nfa()

        # Then
        self.assertTrue(subject.match('abcd'))
        self.assertFalse(subject.match('abc'))
        self.assertFalse(subject.match('abcda'))
        self.assertFalse(subject.match(''))
        self.assertFalse(subject.match('a'))
        self.assertFalse(subject.match('ab'))
        self.assertFalse(subject.match('bc'))
        self.assertFalse(subject.match('d'))

    def test_one_rule_one_char_kleen_star_only(self):
        # Given
        rules_expresions = [['rule_abc', r'a*']]
        rnfa = RegexesToNFA(rules_expresions)

        # When
        subject = rnfa.get_nfa()

        # Then
        self.assertTrue(subject.match(''))
        self.assertTrue(subject.match('a'))
        self.assertTrue(subject.match('aaaaa'))

    def test_one_rule_concatenation_kleen_star(self):
        # Given
        rules_expresions = [['rule_ab', r'ab*']]
        rnfa = RegexesToNFA(rules_expresions)

        # When
        subject = rnfa.get_nfa()

        # Then
        self.assertFalse(subject.match(''))
        self.assertTrue(subject.match('a'))
        self.assertTrue(subject.match('ab'))
        self.assertTrue(subject.match('abbbbb'))
        self.assertFalse(subject.match('b'))
        self.assertEqual((True, 'rule_ab'), subject.match_with_payload('abb'))

    def test_one_rule_concatenation_group_kleen_star(self):
        # Given
        rules_expresions = [['rule_ab', r'a(bc)*d']]
        rnfa = RegexesToNFA(rules_expresions)

        # When
        subject = rnfa.get_nfa()

        # Then
        self.assertFalse(subject.match(''))
        self.assertTrue(subject.match('ad'))
        self.assertTrue(subject.match('abcd'))
        self.assertTrue(subject.match('abcbcd'))

    def test_one_rule_concatenation_zero_or_one(self):
        # Given
        rules_expresions = [['rule_ab', r'ab?']]
        rnfa = RegexesToNFA(rules_expresions)

        # When
        subject = rnfa.get_nfa()

        # Then
        self.assertFalse(subject.match(''))
        self.assertTrue(subject.match('a'))
        self.assertTrue(subject.match('ab'))
        self.assertFalse(subject.match('ababbbb'))
        self.assertFalse(subject.match('b'))
        self.assertEqual((True, 'rule_ab'), subject.match_with_payload('ab'))

    def test_one_rule_concatenation_group_zero_or_one(self):
        # Given
        rules_expresions = [['rule_ab', r'a(bc)?d']]
        rnfa = RegexesToNFA(rules_expresions)

        # When
        subject = rnfa.get_nfa()

        # Then
        self.assertFalse(subject.match(''))
        self.assertTrue(subject.match('ad'))
        self.assertTrue(subject.match('abcd'))
        self.assertFalse(subject.match('abc'))
        self.assertFalse(subject.match('abcbcd'))
        self.assertFalse(subject.match('abcabcd'))

    def test_one_rule_concatenation_one_or_more(self):
        # Given
        rules_expresions = [['rule_ab', r'ab+']]
        rnfa = RegexesToNFA(rules_expresions)

        # When
        subject = rnfa.get_nfa()

        # Then
        self.assertFalse(subject.match(''))
        self.assertFalse(subject.match('a'))
        self.assertTrue(subject.match('ab'))
        self.assertTrue(subject.match('abbbbb'))
        self.assertFalse(subject.match('ababbbb'))
        self.assertFalse(subject.match('b'))
        self.assertEqual((True, 'rule_ab'), subject.match_with_payload('ab'))

    def test_one_rule_concatenation_group_one_or_more(self):
        # Given
        rules_expresions = [['rule_ab', r'a(bc)+d']]
        rnfa = RegexesToNFA(rules_expresions)

        # When
        subject = rnfa.get_nfa()

        # Then
        self.assertFalse(subject.match(''))
        self.assertFalse(subject.match('ad'))
        self.assertFalse(subject.match('abc'))
        self.assertTrue(subject.match('abcd'))
        self.assertTrue(subject.match('abcbcd'))

    def test_one_rule_concatenation_any_char(self):
        # Given
        rules_expresions = [['rule_dotdotdot', r'...']]
        rnfa = RegexesToNFA(rules_expresions)

        # When
        subject = rnfa.get_nfa()

        # Then
        self.assertFalse(subject.match('a'))
        self.assertFalse(subject.match('ab'))
        self.assertFalse(subject.match('abcd'))
        self.assertTrue(subject.match('abc'))
        self.assertTrue(subject.match('efg'))

    def test_one_rule_concatenation_any_char_one_or_more(self):
        # Given
        rules_expresions = [['rule_dotplus', r'.+']]
        rnfa = RegexesToNFA(rules_expresions)

        # When
        subject = rnfa.get_nfa()

        # Then
        self.assertFalse(subject.match(''))
        self.assertTrue(subject.match('a'))
        self.assertTrue(subject.match('ab'))
        self.assertTrue(subject.match('abcd'))
        self.assertTrue(subject.match('efg'))


    def test_one_rule_disjunction_one_char(self):
        # Given
        rules_expresions = [['rule_disjunction', r'a|b|c']]
        rnfa = RegexesToNFA(rules_expresions)

        # When
        subject = rnfa.get_nfa()

        # Then
        self.assertFalse(subject.match(''))
        self.assertTrue(subject.match('a'))
        self.assertTrue(subject.match('b'))
        self.assertTrue(subject.match('c'))
        self.assertFalse(subject.match('d'))

    def test_one_rule_disjunction_concatenation(self):
        # Given
        rules_expresions = [['rule_disjunction', r'abc|def|ijk']]
        rnfa = RegexesToNFA(rules_expresions)

        # When
        subject = rnfa.get_nfa()

        # Then
        self.assertFalse(subject.match(''))
        self.assertTrue(subject.match('abc'))
        self.assertTrue(subject.match('def'))
        self.assertTrue(subject.match('ijk'))

    def test_one_rule_disjunction_concatenation_group(self):
        # Given
        rules_expresions = [['rule_expression', r'1(abc|def|ijk)2']]
        rnfa = RegexesToNFA(rules_expresions)

        # When
        subject = rnfa.get_nfa()

        # Then
        self.assertFalse(subject.match(''))
        self.assertTrue(subject.match('1abc2'))
        self.assertTrue(subject.match('1def2'))
        self.assertTrue(subject.match('1ijk2'))

    def test_one_rule_numeric(self):
        # Given
        rules_expresions = [['rule_numeric', r'[0-9]+(\.[0-9]*)?([eE][\+\-]?[0-9]+)?']]
        rnfa = RegexesToNFA(rules_expresions)

        # When
        subject = rnfa.get_nfa()

        # Then
        self.assertFalse(subject.match(''))
        self.assertTrue(subject.match('1'))
        self.assertTrue(subject.match('123'))
        self.assertTrue(subject.match('123.456'))
        self.assertTrue(subject.match('123.456e2'))
        self.assertTrue(subject.match('123e2'))
        self.assertTrue(subject.match('123e+2'))
        self.assertTrue(subject.match('123E-2'))
        self.assertTrue(subject.match('123.456E+2'))
        self.assertTrue(subject.match('123.456E-2'))

    def test_rules_with_overlap(self):
        # Given
        rules_expresions = [['rule_gt', r'>'], ['rule_gte', r'>=']]
        rnfa = RegexesToNFA(rules_expresions)

        # When
        subject = rnfa.get_nfa()

        # Then
        self.assertEqual((True, 'rule_gt'), subject.match_with_payload('>'))
        self.assertEqual((True, 'rule_gte'), subject.match_with_payload('>='))

    def test_rules_with_ambiguity_should_return_first_specified(self):
        # Given
        rules_expresions = [['rule_1', r'abb'], ['rule_2', r'a*b+']]
        rnfa = RegexesToNFA(rules_expresions)

        # When
        subject = rnfa.get_nfa()

        # Then
        self.assertEqual((True, 'rule_1'), subject.match_with_payload('abb'))
