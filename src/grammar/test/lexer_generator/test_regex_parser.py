import unittest
from frontend_gen.lexer_generator.regex_parser import RegExOp, RegexParser
from frontend_gen.tree.tree import Tree, TreeNode


class TestLexerGeneratorRegEx(unittest.TestCase):

    def test_regex_parser_one_char(self):
        # Given
        expression = 'a'

        root = Tree('a')
        expected = root.dfs(root.get_root())

        # When
        parse_tree = RegexParser.parse(regex=expression)
        subject = parse_tree.dfs(parse_tree.get_root())

        # Then
        self.assertListEqual(subject, expected)

    def test_regex_parser_concatenation_only(self):
        # Given
        expression = 'abc'

        root = Tree(RegExOp.concatenation)
        t = TreeNode(RegExOp.concatenation).add_children([
            TreeNode(RegExOp.concatenation).add_children([TreeNode('a'), TreeNode('b')]), TreeNode('c')])
        root.root = t
        expected = root.dfs(root.get_root())

        # When
        parse_tree = RegexParser.parse(regex=expression)
        subject = parse_tree.dfs(parse_tree.get_root())

        # Then
        self.assertListEqual(subject, expected)

    def test_regex_parser_concatenation_kleene_star_one_char_only(self):
        # Given
        expression = 'a*'

        root = Tree(RegExOp.concatenation)
        t = TreeNode(RegExOp.kleenStar).add_children([TreeNode('a')])
        root.root = t
        expected = root.dfs(root.get_root())

        # When
        parse_tree = RegexParser.parse(regex=expression)
        subject = parse_tree.dfs(parse_tree.get_root())

        # Then
        self.assertListEqual(subject, expected)

    def test_regex_parser_concatenation_kleene_star(self):
        # Given
        expression = 'ab*c'

        root = Tree(RegExOp.concatenation)
        t = TreeNode(RegExOp.concatenation).add_children([
            TreeNode(RegExOp.concatenation).add_children([TreeNode('a'), TreeNode(RegExOp.kleenStar).add_children([TreeNode('b')])]), TreeNode('c')])
        root.root = t
        expected = root.dfs(root.get_root())

        # When
        parse_tree = RegexParser.parse(regex=expression)
        subject = parse_tree.dfs(parse_tree.get_root())

        # Then
        self.assertListEqual(subject, expected)

    def test_regex_parser_concatenation_kleene_star_at_the_end(self):
        # Given
        expression = 'abc*'

        root = Tree(RegExOp.concatenation)
        t = TreeNode(RegExOp.concatenation).add_children([
            TreeNode(RegExOp.concatenation).add_children([TreeNode('a'), TreeNode('b')]), TreeNode(RegExOp.kleenStar).add_children([TreeNode('c')])])
        root.root = t
        expected = root.dfs(root.get_root())

        # When
        parse_tree = RegexParser.parse(regex=expression)
        subject = parse_tree.dfs(parse_tree.get_root())

        # Then
        self.assertListEqual(subject, expected)

    def test_regex_parser_concatenation_one_or_more(self):
        # Given
        expression = 'ab+c'

        root = Tree(RegExOp.concatenation)
        t = TreeNode(RegExOp.concatenation).add_children([
            TreeNode(RegExOp.concatenation).add_children([TreeNode('a'),
                                                          TreeNode(RegExOp.oneOrMore).add_children([TreeNode('b')])]), TreeNode('c')])
        root.root = t
        expected = root.dfs(root.get_root())

        # When
        parse_tree = RegexParser.parse(regex=expression)
        subject = parse_tree.dfs(parse_tree.get_root())

        # Then
        self.assertListEqual(subject, expected)

    def test_regex_parser_concatenation_zero_or_one(self):
        # Given
        expression = 'ab?c'

        root = Tree(RegExOp.concatenation)
        t = TreeNode(RegExOp.concatenation).add_children([
            TreeNode(RegExOp.concatenation).add_children([TreeNode('a'), TreeNode(RegExOp.zeroOrOne).add_children([TreeNode('b')])]), TreeNode('c')])
        root.root = t
        expected = root.dfs(root.get_root())

        # When
        parse_tree = RegexParser.parse(regex=expression)
        subject = parse_tree.dfs(parse_tree.get_root())

        # Then
        self.assertListEqual(subject, expected)

    def test_regex_parser_any_char_one_or_more(self):
        # Given
        expression = '.+'

        root = Tree(RegExOp.concatenation)
        t = TreeNode(RegExOp.oneOrMore).add_children([TreeNode(RegExOp.anyChar)])
        root.root = t
        expected = root.dfs(root.get_root())

        # When
        parse_tree = RegexParser.parse(regex=expression)
        subject = parse_tree.dfs(parse_tree.get_root())

        # Then
        self.assertListEqual(subject, expected)

    def test_regex_parser_concatenation_any_char(self):
        # Given
        expression = '...'

        root = Tree(RegExOp.concatenation)
        root.get_root().add_children([TreeNode(RegExOp.concatenation).add_children(
                                       [TreeNode(RegExOp.anyChar), TreeNode(RegExOp.anyChar)]),
                                      TreeNode(RegExOp.anyChar)])
        expected = root.dfs(root.get_root())

        # When
        parse_tree = RegexParser.parse(regex=expression)
        subject = parse_tree.dfs(parse_tree.get_root())

        # Then
        self.assertListEqual(subject, expected)

    def test_regex_parser_group(self):
        # Given
        expression = 'a(bc)*'

        root = Tree(RegExOp.concatenation)
        t = TreeNode(RegExOp.concatenation).add_children([
            TreeNode('a'), TreeNode(RegExOp.kleenStar).add_children(
                [TreeNode(RegExOp.concatenation).add_children([TreeNode('b'), TreeNode('c')])])
        ])
        root.root = t
        expected = root.dfs(root.get_root())

        # When
        parse_tree = RegexParser.parse(regex=expression)
        subject = parse_tree.dfs(parse_tree.get_root())

        # Then
        self.assertListEqual(subject, expected)

    def test_regex_parser_group_in_the_middle(self):
        # Given
        expression = 'a(bc)+d'

        root = Tree(RegExOp.concatenation)
        t = TreeNode(RegExOp.concatenation).add_children([
                TreeNode(RegExOp.concatenation).add_children([
                    TreeNode('a'), TreeNode(RegExOp.oneOrMore).add_children(
                        [TreeNode(RegExOp.concatenation).add_children([TreeNode('b'), TreeNode('c')])])]),
                TreeNode('d')])
        root.root = t
        expected = root.dfs(root.get_root())

        # When
        parse_tree = RegexParser.parse(regex=expression)
        subject = parse_tree.dfs(parse_tree.get_root())

        # Then
        self.assertListEqual(subject, expected)


    def test_regex_parser_disjunction(self):
        # Given
        expression = 'a|b'

        root = Tree(RegExOp.disjunction)
        root.get_root().add_children([TreeNode('a'), TreeNode('b')])
        expected = root.dfs(root.get_root())

        # When
        parse_tree = RegexParser.parse(regex=expression)
        subject = parse_tree.dfs(parse_tree.get_root())

        # Then
        self.assertListEqual(subject, expected)

    def test_regex_parser_multiple_disjunctions(self):
        # Given
        expression = 'a|b|c|d'

        root = Tree(RegExOp.disjunction)

        root.get_root().add_children([TreeNode('a'), TreeNode('b'), TreeNode('c'), TreeNode('d')])
        expected = root.dfs(root.get_root())

        # When
        parse_tree = RegexParser.parse(regex=expression)
        subject = parse_tree.dfs(parse_tree.get_root())

        # Then
        self.assertListEqual(subject, expected)

    def test_regex_parser_multiple_disjunctions_with_concatenation(self):
        # Given
        expression = 'a|bc|d|e'

        root = Tree(RegExOp.disjunction)

        root.get_root().add_children([TreeNode('a'), TreeNode(RegExOp.concatenation).add_children([TreeNode('b')
                                      , TreeNode('c')]), TreeNode('d'), TreeNode('e')])
        expected = root.dfs(root.get_root())

        # When
        parse_tree = RegexParser.parse(regex=expression)
        subject = parse_tree.dfs(parse_tree.get_root())

        # Then
        self.assertListEqual(subject, expected)

    def test_regex_parser_group_disjunction(self):
        # Given
        expression = 'a(b|c|d|e)'

        root = Tree(RegExOp.concatenation)
        t = TreeNode(RegExOp.concatenation).add_children([
                TreeNode('a'), TreeNode(RegExOp.disjunction).add_children(
                    [TreeNode('b'), TreeNode('c'), TreeNode('d'), TreeNode('e')])])

        root.root = t
        expected = root.dfs(root.get_root())

        # When
        parse_tree = RegexParser.parse(regex=expression)
        subject = parse_tree.dfs(parse_tree.get_root())

        # Then
        self.assertListEqual(subject, expected)

    def test_regex_parser_bracket_base_case(self):
        # Given
        expression = '[a]'

        root = Tree(RegExOp.disjunction)
        root.get_root().add_children([TreeNode('a')])

        expected = root.dfs(root.get_root())

        # When
        parse_tree = RegexParser.parse(regex=expression)
        subject = parse_tree.dfs(parse_tree.get_root())

        # Then
        self.assertListEqual(subject, expected)

    def test_regex_parser_bracket_implicit_disjunction_only(self):
        # Given
        expression = '[abc]'

        root = Tree(RegExOp.disjunction)
        root.get_root().add_children([TreeNode('a'), TreeNode('b'), TreeNode('c')])

        expected = root.dfs(root.get_root())

        # When
        parse_tree = RegexParser.parse(regex=expression)
        subject = parse_tree.dfs(parse_tree.get_root())

        # Then
        self.assertListEqual(subject, expected)

    def test_regex_parser_bracket_explicit_disjunction_only(self):
        # Given
        expression = '[a|b|c]'

        root = Tree(RegExOp.disjunction)
        root.get_root().add_children([TreeNode('a'), TreeNode('b'), TreeNode('c')])

        expected = root.dfs(root.get_root())

        # When
        parse_tree = RegexParser.parse(regex=expression)
        subject = parse_tree.dfs(parse_tree.get_root())

        # Then
        self.assertListEqual(subject, expected)

    def test_regex_parser_bracket_range_only(self):
        # Given
        expression = '[a-c]'

        root = Tree(RegExOp.disjunction)
        root.get_root().add_children([TreeNode('a'), TreeNode('b'), TreeNode('c')])

        expected = root.dfs(root.get_root())

        # When
        parse_tree = RegexParser.parse(regex=expression)
        subject = parse_tree.dfs(parse_tree.get_root())

        # Then
        self.assertListEqual(subject, expected)

    def test_regex_parser_bracket_reverse_range_only(self):
        # Given
        expression = '[c-a]'

        root = Tree(RegExOp.disjunction)
        root.get_root().add_children([TreeNode('c'), TreeNode('b'), TreeNode('a')])

        expected = root.dfs(root.get_root())

        # When
        parse_tree = RegexParser.parse(regex=expression)
        subject = parse_tree.dfs(parse_tree.get_root())

        # Then
        self.assertListEqual(subject, expected)

    def test_regex_parser_bracket_multiple_ranges(self):
        # Given
        expression = '[a-cm-o]'

        root = Tree(RegExOp.disjunction)
        root.get_root().add_children([TreeNode('a'), TreeNode('b'), TreeNode('c'), TreeNode('m'), TreeNode('n'), TreeNode('o')])

        expected = root.dfs(root.get_root())

        # When
        parse_tree = RegexParser.parse(regex=expression)
        subject = parse_tree.dfs(parse_tree.get_root())

        # Then
        self.assertListEqual(subject, expected)

    def test_regex_parser_bracket_kleen(self):
        # Given
        expression = '[a-c]*'

        root = Tree(RegExOp.kleenStar)
        root.get_root().add_child(TreeNode(RegExOp.disjunction).add_children([TreeNode('a'), TreeNode('b'), TreeNode('c')]))

        expected = root.dfs(root.get_root())

        # When
        parse_tree = RegexParser.parse(regex=expression)
        subject = parse_tree.dfs(parse_tree.get_root())

        # Then
        self.assertListEqual(subject, expected)

    def test_regex_parser_concatenation_and_bracket(self):
        # Given
        expression = 'a[bc]d'

        root = Tree(RegExOp.concatenation)
        t = TreeNode(RegExOp.concatenation).add_children([
            TreeNode(RegExOp.concatenation).add_children([TreeNode('a'),
                                                          TreeNode(RegExOp.disjunction).add_children([TreeNode('b'), TreeNode('c')])])
            , TreeNode('d')])
        root.root = t
        expected = root.dfs(root.get_root())

        # When
        parse_tree = RegexParser.parse(regex=expression)
        subject = parse_tree.dfs(parse_tree.get_root())

        # Then
        self.assertListEqual(subject, expected)

    def test_regex_parse_char_escape(self):
        # Given
        expression = '\\t'

        root = Tree('\t')
        expected = root.dfs(root.get_root())

        # When
        parse_tree = RegexParser.parse(regex=expression)
        subject = parse_tree.dfs(parse_tree.get_root())

        # Then
        self.assertListEqual(subject, expected)

    def test_regex_escape_char_in_brackets(self):
        # Given
        expression = '[\\t| |\\n]+'

        root = Tree(RegExOp.oneOrMore)
        root.get_root().add_child(TreeNode(RegExOp.disjunction).add_children([TreeNode('\t'), TreeNode(' '), TreeNode('\n')]))

        expected = root.dfs(root.get_root())

        # When
        parse_tree = RegexParser.parse(regex=expression)
        subject = parse_tree.dfs(parse_tree.get_root())

        # Then
        self.assertListEqual(subject, expected)

    @unittest.skip('Feature not yet implemented')
    def test_regex_escape_char_in_brackets_range(self):
        # Given
        expression = '[\\t-\\n]+'

        root = Tree(RegExOp.oneOrMore)
        root.get_root().add_child(TreeNode(RegExOp.disjunction).add_children([TreeNode('\t'), TreeNode('\n')]))

        expected = root.dfs(root.get_root())

        # When
        parse_tree = RegexParser.parse(regex=expression)
        subject = parse_tree.dfs(parse_tree.get_root())

        # Then
        self.assertListEqual(subject, expected)

    def test_regex_parse_special_char_escape(self):
        # Given
        expression = '\.\*'

        root = Tree(RegExOp.concatenation)
        root.get_root().add_children([TreeNode('.'), TreeNode('*')])
        expected = root.dfs(root.get_root())

        # When
        parse_tree = RegexParser.parse(regex=expression)
        subject = parse_tree.dfs(parse_tree.get_root())

        # Then
        self.assertListEqual(subject, expected)