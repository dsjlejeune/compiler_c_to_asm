import unittest

from frontend_gen.finite_automaton.states import *


class TestStates(unittest.TestCase):

    def test_states_add_transition_without_epsilon_transitions(self):
        # Given
        s1 = State('1')
        s2 = State('2')
        s3 = State('3')
        s4 = State('4', accepting=True)

        # When
        s1.add_transition(s2, 'a')
        s1.add_transition(s3, 'b')
        s3.add_transition(s4, '1')

        # Then
        self.assertListEqual(list(s1.transition.keys()), ['a', 'b'])
        self.assertListEqual(list(s2.transition.keys()), [])
        self.assertListEqual(list(s3.transition.keys()), ['1'])

    def test_states_add_transition_with_epsilon_transitions(self):
        # Given a set of states/transition for the language (a|b1)c
        s1 = State('1')
        s2 = State('2')
        s3 = State('3')
        s4 = State('4')
        s5 = State('5')
        s6 = State('6')
        s7 = State('7', accepting=True)

        # When
        s1.add_transition(s2, 'a')
        s1.add_transition(s3, 'b')

        s2.add_transition(s5, '')

        s3.add_transition(s4, '1')

        s4.add_transition(s5, '')

        s5.add_transition(s6, '')

        s6.add_transition(s7, 'c')

        # Then
        self.assertListEqual(list(s1.transition.keys()), ['a', 'b'])
        self.assertListEqual(list(s2.transition.keys()), [''])
        self.assertListEqual(list(s3.transition.keys()), ['1'])
        self.assertListEqual(list(s4.transition.keys()), [''])
        self.assertListEqual(list(s5.transition.keys()), [''])
        self.assertListEqual(list(s6.transition.keys()), ['c'])
        self.assertListEqual(list(s7.transition.keys()), [])

        self.assertFalse(s1.accepting)
        self.assertFalse(s2.accepting)
        self.assertFalse(s3.accepting)
        self.assertFalse(s4.accepting)
        self.assertFalse(s5.accepting)
        self.assertFalse(s6.accepting)
        self.assertTrue(s7.accepting)


    def test_states_not_equal_on_different_identifiers(self):
        # Given
        identifier1 = '1'
        identifier2 = '2'

        # When
        s1 = State(identifier1)
        s2 = State(identifier2)

        # Then
        self.assertNotEqual(s1, s2)

    def test_states_equal_on_same_identifier(self):
        # Given
        identifier = '1'

        # When
        s1 = State(identifier)
        s2 = State(identifier)

        # Then
        self.assertEqual(s1, s2)

    def test_states_same_hash_on_same_identifier(self):
        # Given
        s1 = State('1')
        s1_prime = State('1')
        s2 = State('2')

        # When
        subject = {}
        subject[s1] = 'a'
        subject[s2] = 'b'
        subject[s1_prime] = 'c'

        # Then
        self.assertEqual(len(subject), 2)
        self.assertCountEqual(list(subject.keys()), [s1, s2])
        self.assertCountEqual(list(subject.values()), ['b', 'c'])


    def test_setofstates_add_transitions(self):
        # Given
        s1 = State('1')
        s2 = State('2')
        s3 = State('3')
        s4 = State('4')
        s5 = State('5')

        set_s1 = SetOfStates([s1, s2])
        set_s2 = SetOfStates([s3, s4, s5])

        # When
        set_s1.add_transition(set_s2, 'a')

        # Then
        self.assertEqual(len(set_s1.transition), 1)
        self.assertEqual(set_s1.transition['a'], set_s2)

    def test_setofstates_sameness_ordered(self):
        # Given
        s1 = State('1')
        s2 = State('2')
        s3 = State('1')
        s4 = State('2')

        # When
        set_s1 = SetOfStates([s1, s2])
        set_s2 = SetOfStates([s3, s4])

        # Then
        self.assertEqual(set_s1, set_s2)

    def test_setofstates_sameness_unordered(self):
        # Given
        s1 = State('1')
        s2 = State('2')
        s3 = State('1')
        s4 = State('2')

        # When
        set_s1 = SetOfStates([s1, s2])
        set_s2 = SetOfStates([s4, s3])

        # Then
        self.assertEqual(set_s1, set_s2)


if __name__ == '__main__':
    unittest.main()