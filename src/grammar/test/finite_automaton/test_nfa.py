import unittest

from frontend_gen.finite_automaton.states import *
from frontend_gen.finite_automaton.nfa import NFA


class TestNFA(unittest.TestCase):

    def test_add_transition(self):
        # Given
        s2 = State('2', True)

        # When
        nfa = NFA()

        nfa.add_transition(nfa.init, s2, '1')
        nfa.add_transition(nfa.init, s2, '2')
        nfa.add_transition(s2, nfa.init, '')

        # Then
        self.assertEqual(len(nfa.init.transition.keys()), 2)
        self.assertTrue('1' in nfa.init.transition)
        self.assertTrue('2' in nfa.init.transition)
        self.assertEqual(len(nfa.init.transition['1']), 1)
        self.assertEqual(nfa.init.transition['1'][0], s2)
        self.assertEqual(len(nfa.init.transition['1'][0].transition['']), 1)
        self.assertEqual(nfa.init.transition['1'][0].transition[''][0], nfa.init)

    def test_epsilon_closure(self):
        # Given
        s2 = State('2', True)
        s3 = State('3', True)
        s4 = State('4', True)
        s5 = State('5')
        s6 = State('6')
        s7 = State('7')

        nfa = NFA()

        nfa.add_transition(nfa.init, s2, '')

        nfa.add_transition(s2, s3, '')
        nfa.add_transition(s3, s4, 'a')
        nfa.add_transition(s4, s7, '')

        nfa.add_transition(s2, s5, '')
        nfa.add_transition(s5, s6, 'b')
        nfa.add_transition(s6, s7, '')

        nfa.add_transition(s7, s2, '')

        # When
        subject = nfa.epsilon_closure([nfa.init])

        # Then
        self.assertListEqual(subject, [nfa.init, s2, s3, s5])

    def test_record_accepting(self):
        # Given
        s2 = State('2', True)
        s3 = State('3', True)
        s4 = State('4', True)
        s5 = State('5')
        s6 = State('6')
        s7 = State('7')

        # When
        subject = NFA()

        subject.add_transition(subject.init, s2, '')

        subject.add_transition(s2, s3, '')
        subject.add_transition(s3, s4, 'a')
        subject.add_transition(s4, s7, '')

        subject.add_transition(s2, s5, '')
        subject.add_transition(s5, s6, 'b')
        subject.add_transition(s6, s7, '')

        subject.add_transition(s7, s2, '')

        # Then
        self.assertCountEqual(subject.accepting_states, [s2, s3, s4])

    def test_move(self):
        # Given
        s2 = State('2', True)
        s3 = State('3', True)
        s4 = State('4', True)
        s5 = State('5')
        s6 = State('6')
        s7 = State('7')

        subject = NFA()

        subject.add_transition(subject.init, s2, '')

        subject.add_transition(s2, s3, '')
        subject.add_transition(s3, s4, 'a')
        subject.add_transition(s4, s7, '')

        subject.add_transition(s2, s5, '')
        subject.add_transition(s5, s6, 'b')
        subject.add_transition(s6, s7, '')

        subject.add_transition(s7, s2, '')

        # When
        subject = subject.move([s2, s3, s5], 'a')

        # Then
        self.assertListEqual(subject, [s4])

    def test_match(self):
        """
        Test the language (1|2|a|b)+
        """

        # Given
        nfa = NFA()
        s2 = State('2', True)
        s3 = State('3', True)
        nfa.add_transition(nfa.init, s2, '1')
        nfa.add_transition(nfa.init, s2, '2')
        nfa.add_transition(s2, nfa.init, '')

        nfa.add_transition(nfa.init, s3, 'a')
        nfa.add_transition(nfa.init, s3, 'b')
        nfa.add_transition(s3, nfa.init, '')

        # Then
        self.assertTrue(nfa.match('12abb'))
        self.assertFalse(nfa.match('1cab'))
        self.assertTrue(nfa.match('1a2b'))
        self.assertTrue(nfa.match('a2b111'))
        self.assertTrue(nfa.match('1'))
        self.assertTrue(nfa.match('a'))
        self.assertFalse(nfa.match(''))

    def test_match_with_payload(self):
        """
        Test the language (1|2|a|b)+
        """

        # Given
        nfa = NFA()
        s2 = State('2', True, payload='accepting_state_1')
        s3 = State('3', True, payload='accepting_state_2')
        nfa.add_transition(nfa.init, s2, '1')
        nfa.add_transition(nfa.init, s2, '2')
        nfa.add_transition(s2, nfa.init, '')

        nfa.add_transition(nfa.init, s3, 'a')
        nfa.add_transition(nfa.init, s3, 'b')
        nfa.add_transition(s3, nfa.init, '')

        # Then
        self.assertEqual((True, 'accepting_state_2'), nfa.match_with_payload('12abb'))
        self.assertEqual((True, 'accepting_state_1'), nfa.match_with_payload('abb12'))
        self.assertFalse(nfa.match('1cab'))

    def test_match_any_char(self):
        """
        Test the language '.+'
        """

        # Given
        nfa = NFA()
        s2 = State('2')
        s3 = State('3')
        s4 = State('4', True)
        nfa.add_transition(nfa.init, s2, '\'')
        nfa.add_transition(s2, s3, SpecialTransition.ANY)
        nfa.add_transition(s3, s2, '')
        nfa.add_transition(s3, s4, '\'')

        # Then
        self.assertTrue(nfa.match("'abc'"))
        self.assertTrue(nfa.match("'a'"))
        self.assertTrue(nfa.match("'.'"))
        self.assertFalse(nfa.match("''"))
        self.assertFalse(nfa.match(''))

    def test_to_dfa(self):
        # Given an NFA for the language (a|b)*abb
        s2 = State('2')
        s3 = State('3')
        s4 = State('4')
        s5 = State('5')
        s6 = State('6')
        s7 = State('7')
        s8 = State('8')
        s9 = State('9')
        s10 = State('10')
        s11 = State('11', True)

        nfa = NFA()

        nfa.add_transition(nfa.init, s2, '')
        nfa.add_transition(nfa.init, s8, '')

        nfa.add_transition(s2, s3, '')
        nfa.add_transition(s3, s4, 'a')
        nfa.add_transition(s4, s7, '')

        nfa.add_transition(s2, s5, '')
        nfa.add_transition(s5, s6, 'b')
        nfa.add_transition(s6, s7, '')

        nfa.add_transition(s7, s2, '')
        nfa.add_transition(s7, s8, '')

        nfa.add_transition(s8, s9, 'a')
        nfa.add_transition(s9, s10, 'b')

        nfa.add_transition(s10, s11, 'b')

        # When
        subject = nfa.to_dfa()

        # Then
        self.assertEqual(subject.init, SetOfStates([nfa.init, s2, s3, s5, s8]))
        self.assertFalse(subject.init.accepting)

        self.assertListEqual(list(subject.init.transition.keys()), ['a', 'b'])
        self.assertEqual(subject.init.transition['a'], SetOfStates([s2, s3, s4, s5, s7, s8, s9]))
        self.assertFalse(subject.init.transition['a'].accepting)

        self.assertEqual(subject.init.transition['b'], SetOfStates([s2, s3, s5, s6, s7, s8]))
        self.assertFalse(subject.init.transition['b'].accepting)

        self.assertListEqual(list(subject.init.transition['a'].transition.keys()), ['a', 'b'])
        self.assertEqual(subject.init.transition['a'].transition['b'], SetOfStates([s2, s3, s5, s6, s7, s8, s10]))
        self.assertFalse(subject.init.transition['a'].transition['b'].accepting)

        self.assertListEqual(list(subject.init.transition['a'].transition['b'].transition.keys()), ['a', 'b'])
        self.assertEqual(subject.init.transition['a'].transition['b'].transition['b'],
                         SetOfStates([s2, s3, s5, s6, s7, s8, s11]))
        self.assertTrue(subject.init.transition['a'].transition['b'].transition['b'].accepting)


if __name__ == '__main__':
    unittest.main()
