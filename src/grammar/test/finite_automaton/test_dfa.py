import unittest

from frontend_gen.finite_automaton.dfa import *
from frontend_gen.finite_automaton.states import State


class TestDFA(unittest.TestCase):
    def test_add_transition(self):
        # Given
        s1 = State('1')
        s2 = State('2')
        s3 = State('3')
        s4 = State('4', True)

        # When
        subject = DFA(s1) # DFA for langage (a|b)b
        subject.add_transition(s1, s2, 'a')
        subject.add_transition(s1, s3, 'b')
        subject.add_transition(s2, s4, 'b')
        subject.add_transition(s3, s4, 'b')

        # Then
        self.assertEqual(subject.init, s1)
        self.assertEqual(len(subject.init.transition), 2)

    def test_match(self):
        # Given
        s1 = State('1')
        s2 = State('2')
        s3 = State('3')
        s4 = State('4', True)

        # When
        subject = DFA(s1) # DFA for langage (a|b)b
        subject.add_transition(s1, s2, 'a')
        subject.add_transition(s1, s3, 'b')
        subject.add_transition(s2, s4, 'b')
        subject.add_transition(s3, s4, 'b')

        # Then
        self.assertTrue(subject.match('ab'))
        self.assertTrue(subject.match('bb'))
        self.assertFalse(subject.match(''))
        self.assertFalse(subject.match('a'))
        self.assertFalse(subject.match('b'))