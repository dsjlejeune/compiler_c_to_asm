import unittest

from frontend_gen.tree.tree import *


class TestTree(unittest.TestCase):

    def test_tree_insert(self):
        # Given
        node_left = TreeNode(value='b')
        node_middle = TreeNode(value='c')
        node_right = TreeNode(value='d')

        # When
        subject = Tree(root_value='a')
        root_node = subject.get_root()
        subject.add_child_to_node(root_node, node_left)
        subject.add_child_to_node(root_node, node_middle)
        subject.add_child_to_node(root_node, node_right)

        # Then
        self.assertCountEqual([node_left, node_middle, node_right], subject.get_children(root_node))

    def test_tree_dfs(self):
        # Given
        t = Tree(root_value='a')
        node_left = TreeNode(value='b')
        node_right = TreeNode(value='c')
        sub_node_left = TreeNode(value='d')
        sub_sub_node_left = TreeNode(value='e')

        root = t.get_root()
        node_left.add_child(sub_node_left)
        sub_node_left.add_child(sub_sub_node_left)

        root.add_child(node_left)
        root.add_child(node_right)

        expected = [root, [node_left, [sub_node_left, [sub_sub_node_left]]], [node_right]]

        # When
        subject = t.dfs(root)

        # Then
        self.assertEqual(expected, subject)

    def test_tree_node_equality_when_same(self):
        # Given
        value_1 = 'a'
        value_2 = 'a'

        children_1 = ['c', 'd']
        children_2 = ['c', 'd']

        # When
        n1 = TreeNode(value_1, children=children_1)
        n2 = TreeNode(value_2, children=children_2)

        # Then
        self.assertEqual(n1, n2)

    def test_tree_node_equality_when_different_children(self):
        # Given
        value_1 = 'a'
        value_2 = 'a'

        children_1 = ['c', 'd']
        children_2 = ['c', 'd', 'e']

        # When
        n1 = TreeNode(value_1, children=children_1)
        n2 = TreeNode(value_2, children=children_2)

        # Then
        self.assertNotEqual(n1, n2)

    def test_tree_node_equality_when_different_values(self):
        # Given
        value_1 = 'a'
        value_2 = 'aa'

        children_1 = ['c', 'd']
        children_2 = ['c', 'd']

        # When
        n1 = TreeNode(value_1, children=children_1)
        n2 = TreeNode(value_2, children=children_2)

        # Then
        self.assertNotEqual(n1, n2)