import unittest
from unittest.mock import patch, mock_open

from frontend_gen.parser_generator.bnf import *


class TestBNF(unittest.TestCase):

    def test_parsing_production_terminal(self):
        # Given
        test_string = '"a simple string"'
        grammar = '<test> ::= {}'.format(test_string)

        # When
        subject = []
        with patch('builtins.open', mock_open(read_data=grammar)):
            bnf = BNF(filename_grammar='test_grammar.bnf')
            subject = bnf._parse_production_rule(test_string)

        # Then
        self.assertEqual(subject[0], test_string)

    def test_parsing_production_terminal_with_escaped_quote(self):
        # Given
        test_string = '"a quote \\" is hidding"'
        grammar = '<test> ::= {}'.format(test_string)

        # When
        subject = []
        with patch('builtins.open', mock_open(read_data=grammar)):
            bnf = BNF(filename_grammar='test_grammar.bnf')
            subject = bnf._parse_production_rule(test_string)

        # Then
        self.assertEqual(subject[0], test_string)

    def test_parsing_production_nonterminal(self):
        # Given
        test_string = '<non terminal>'
        grammar = '<test> ::= {}'.format(test_string)

        # When
        subject = []
        with patch('builtins.open', mock_open(read_data=grammar)):
            bnf = BNF(filename_grammar='test_grammar.bnf')
            subject = bnf._parse_production_rule(test_string)

        # Then
        self.assertEqual(subject[0], test_string)

    def test_parsing_production_disjunction(self):
        # Given
        test_string = ' "terminal" | <non_terminal>'
        grammar = '<test> ::= {}'.format(test_string)

        # When
        subject = []
        with patch('builtins.open', mock_open(read_data=grammar)):
            bnf = BNF(filename_grammar='test_grammar.bnf')
            subject = bnf._parse_production_rule(test_string)

        # Then
        self.assertEqual(len(subject), 2)
        self.assertEqual(subject[0], '"terminal"')
        self.assertEqual(subject[1], '<non_terminal>')

    def test_parsing_production_broken_string(self):
        # Given
        test_string = ' "terminal" | <broken_non_terminal'

        grammar = '<test> ::= {}'.format(test_string)

        # When
        subject = []
        with patch('builtins.open', mock_open(read_data=grammar)):
            bnf = BNF(filename_grammar='test_grammar.bnf')

            # Then
            with self.assertRaisesRegex(Exception, 'Syntax error.*'):
                bnf._parse_production_rule(test_string)

    def test_BNF_with_recursion_rule(self):
        # Given
        grammar = '<recursion_a> ::= <recursion_a> "a" | "a"'

        # When
        with patch('builtins.open', mock_open(read_data=grammar)):
            bnf = BNF(filename_grammar='test_grammar.bnf')
            bnf.read_rules()

            # Then
            self.assertEqual(bnf.symbol_table.has_nonterminal('recursion_a'), True)
            subject = bnf.symbol_table.get_nonterminal('recursion_a')

            self.assertEqual(subject.production.conjunctions[0].rules[0], subject)

    def test_BNF_with_disjunction(self):
        # Given
        grammar = '<one> ::= "1"\n<two> ::= "2"\n<one_two> ::= <one> | <two>'

        # When
        with patch('builtins.open', mock_open(read_data=grammar)):
            subject = BNF(filename_grammar='test_grammar.bnf')
            subject.read_rules()

            # Then
            self.assertEqual(subject.symbol_table.has_nonterminal('one'), True)
            self.assertEqual(subject.symbol_table.has_nonterminal('two'), True)
            self.assertEqual(subject.symbol_table.has_nonterminal('one_two'), True)

            self.assertEqual(subject.symbol_table.get_nonterminal('one').production.rules[0].symbol, "1")
            self.assertEqual(subject.symbol_table.get_nonterminal('two').production.rules[0].symbol, "2")

            self.assertEqual(len(subject.symbol_table.get_nonterminal('one_two').production.conjunctions), 2)
            self.assertEqual(
                subject.symbol_table.get_nonterminal('one_two').production.conjunctions[0].rules[0].get_name(), "one")
            self.assertEqual(
                subject.symbol_table.get_nonterminal('one_two').production.conjunctions[1].rules[0].get_name(), "two")

    def test_BNF_ignore_space_and_comments(self):
        # Given
        grammar = '<one> ::= "1"\n#this is a comment\n\n\n<two> ::= "2"\n<one_two> ::= <one> | <two>\n'

        # When
        with patch('builtins.open', mock_open(read_data=grammar)):
            subject = BNF(filename_grammar='test_grammar.bnf')
            subject.read_rules()

            # Then
            self.assertEqual(subject.symbol_table.has_nonterminal('one'), True)
            self.assertEqual(subject.symbol_table.has_nonterminal('two'), True)
            self.assertEqual(subject.symbol_table.has_nonterminal('one_two'), True)

            self.assertEqual(subject.symbol_table.get_nonterminal('one').production.rules[0].symbol, "1")
            self.assertEqual(subject.symbol_table.get_nonterminal('two').production.rules[0].symbol, "2")

            self.assertEqual(len(subject.symbol_table.get_nonterminal('one_two').production.conjunctions), 2)
            self.assertEqual(
                subject.symbol_table.get_nonterminal('one_two').production.conjunctions[0].rules[0].get_name(), "one")
            self.assertEqual(
                subject.symbol_table.get_nonterminal('one_two').production.conjunctions[1].rules[0].get_name(), "two")

    def test_BNF_use_rule_before_definition(self):
        # Given
        grammar = '<rule_1> ::= <rule_2><rule_3>\n<rule_2> ::= "a"\n<rule_3> ::= "b"\n<rule_4> ::= <rule_2> "c"'

        # When
        with patch('builtins.open', mock_open(read_data=grammar)):
            subject = BNF(filename_grammar='test_grammar.bnf')
            subject.read_rules()

            # Then
            self.assertEqual(subject.symbol_table.has_nonterminal('rule_1'), True)
            self.assertEqual(subject.symbol_table.has_nonterminal('rule_4'), True)
            self.assertEqual(subject.symbol_table.has_nonterminal('rule_2'), True)
            self.assertEqual(subject.symbol_table.has_nonterminal('rule_3'), True)

            self.assertEqual(subject.symbol_table.get_nonterminal('rule_1').production.rules[0].get_name(), "rule_2")
            self.assertEqual(subject.symbol_table.get_nonterminal('rule_1').production.rules[1].get_name(), "rule_3")

            self.assertEqual(subject.symbol_table.get_nonterminal('rule_1').production.rules[0],
                             subject.symbol_table.get_nonterminal('rule_4').production.rules[0])

    def test_BNF_use_undefined_rule(self):
        # Given
        grammar = '<rule_1> ::= <rule_2><rule_3>\n<rule_2> ::= "a"\n<rule_4> ::= <rule_2> "c"'

        # When
        with patch('builtins.open', mock_open(read_data=grammar)):
            subject = BNF(filename_grammar='test_grammar.bnf')
            # Then
            with self.assertRaisesRegex(Exception, '.*<rule_3> is used but never defined.*') as context:
                subject.read_rules()

    def test_BNF_redefine_rule(self):
        # Given
        grammar = '<rule_1> ::= <rule_2><rule_3>\n<rule_2> ::= "a"\n<rule_3> ::= <rule_2> "c"\n<rule_1> ::= <rule_2>'

        # When
        with patch('builtins.open', mock_open(read_data=grammar)):
            subject = BNF(filename_grammar='test_grammar.bnf')
            # Then
            with self.assertRaisesRegex(Exception, '.*<rule_1> has already been defined.*') as context:
                subject.read_rules()

    def test_BNF_with_lexer(self):
        # Given
        lexer_rules = [['lexer_terminal', 'terminal_from_lexer']]
        test_string = '"terminal_from_lexer" | <non_terminal> <lexer_terminal>'
        grammar = 'import \"test_lexer.reg\"\n<non_terminal> ::= {}'.format(test_string)

        # When
        with patch('builtins.open', mock_open(read_data=grammar)), \
                patch.object(ReadLexRule, 'get_rules', return_value=lexer_rules):
            # Note that both the parser and lexer will read the same content provided by the mock.
            # For now as the syntax is the same, so it should not be a problem
            subject = BNF(filename_grammar='test_grammar.bnf')
            subject.read_rules()

        # Then
        self.assertEqual(subject.symbol_table.has_terminal('terminal_from_lexer'), True)
        self.assertEqual(subject.symbol_table.has_terminal('lexer_terminal'), True)

        self.assertEqual(subject.symbol_table.get_terminal('terminal_from_lexer').get_name(),
                         subject.symbol_table.get_terminal('lexer_terminal').get_name())

        self.assertEqual(subject.symbol_table.get_nonterminal('non_terminal')
                         .production.conjunctions[0].rules[0].get_name(), 'lexer_terminal')

        self.assertEqual(subject.symbol_table.get_nonterminal('non_terminal')
                         .production.conjunctions[1].rules[1].get_name(), 'lexer_terminal')

    def test_BNF_augmented_grammar(self):
        # Given
        grammar = '<rule_1> ::= <rule_2><rule_3>\n<rule_2> ::= "a"\n<rule_3> ::= <rule_2> "c"\n<rule_4> ::= <rule_2>'
        original_start_symbol = 'rule_1'
        expected_new_start_symbol = 'rule_1_augmented_grammar_rule'

        # When
        with patch('builtins.open', mock_open(read_data=grammar)):
            subject = BNF(filename_grammar='test_grammar.bnf', start_symbol=original_start_symbol)
            subject.read_rules()
            subject.augment_grammar()

        subject_production = subject.symbol_table\
            .get_nonterminal(subject.start_symbol)\
            .production

        # Then
        self.assertEqual(subject.start_symbol, expected_new_start_symbol)
        self.assertEqual(subject_production.rules_name, [original_start_symbol])
        self.assertEqual(len(subject_production.rules), 1)
        self.assertEqual(subject_production.rules[0].name, original_start_symbol)

    def test_BNF_augmented_grammar_without_start_symbol(self):
        # Given
        grammar = '<rule_1> ::= <rule_2><rule_3>\n<rule_2> ::= "a"\n<rule_3> ::= <rule_2> "c"\n<rule_4> ::= <rule_2>'
        with patch('builtins.open', mock_open(read_data=grammar)):
            subject = BNF(filename_grammar='test_grammar.bnf', start_symbol=None)
            subject.read_rules()

        # When
        with self.assertRaisesRegex(AttributeError, '.*start symbol needs to be set.*'):
            subject.augment_grammar()

    def test_BNF_augmented_grammar_twice(self):
        # Given
        grammar = '<rule_1> ::= <rule_2><rule_3>\n<rule_2> ::= "a"\n<rule_3> ::= <rule_2> "c"\n<rule_4> ::= <rule_2>'
        with patch('builtins.open', mock_open(read_data=grammar)):
            subject = BNF(filename_grammar='test_grammar.bnf', start_symbol='rule_1')
            subject.read_rules()

        # When
        with self.assertRaisesRegex(AttributeError, '.*grammar is already augmented.*'):
            subject.augment_grammar()
            subject.augment_grammar()


if __name__ == '__main__':
    unittest.main()
