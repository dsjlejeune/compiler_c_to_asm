import unittest

from frontend_gen.parser_generator.item_set import ItemSet


class TestItemSet(unittest.TestCase):

    def test_ItemSet_right_format(self):
        # Given
        items = [('rule_1', 0, 0), ('rule_2', 0, 0), ('rule_2', 1, 0)]

        # When
        subject = ItemSet(items)

        # Then
        self.assertCountEqual(list(subject.items), items)

    def test_ItemSet_wrong_format(self):
        # Given
        items = [('rule_1', 0, 0), ('rule_2', 0), ('rule_2', 1, 0)]

        # When, then
        self.assertRaises(ValueError, ItemSet, items)

    def test_ItemSet_compare_equal_sets(self):
        # Given
        items_1 = [('rule_1', 0, 0), ('rule_2', 0, 0), ('rule_2', 1, 0)]
        items_2 = [('rule_3', 0, 2), ('rule_4', 0, 3), ('rule_5', 1, 4)]
        items_3 = [('rule_3', 0, 2), ('rule_5', 1, 4), ('rule_4', 0, 3)]

        # When
        itemset_1 = ItemSet(items_1)
        itemset_2 = ItemSet(items_2)
        itemset_3 = ItemSet(items_3)

        # Then
        self.assertNotEqual(itemset_1, itemset_2)
        self.assertEqual(itemset_2, itemset_3)

    def test_ItemSet_is_hashable(self):
        # Given
        items_1 = [('rule_1', 0, 0), ('rule_2', 0, 0), ('rule_2', 1, 0)]
        items_2 = [('rule_3', 0, 2), ('rule_4', 0, 3), ('rule_5', 1, 4)]

        itemset_1 = ItemSet(items_1)
        itemset_2 = ItemSet(items_2)
        itemset_3 = ItemSet(items_2)

        # When
        subject = {itemset_1: 1, itemset_2: 2}

        # Then
        self.assertEqual(subject[itemset_1], 1)
        self.assertEqual(subject[itemset_2], 2)
        self.assertTrue(itemset_3 in subject)
