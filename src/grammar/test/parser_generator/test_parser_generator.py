import unittest
from unittest.mock import patch, mock_open

# from frontend_gen.finite_automaton.nfa import *
from frontend_gen.parser_generator.generator.cpp.cpp_generator import *
from frontend_gen.parser_generator.parser_generator import *

from pathlib import Path
import os

class TestParserGenerator(unittest.TestCase):

    def setUp(self):
        current_path = Path(os.path.dirname(os.path.realpath(__file__)))
        self.path_grammar = current_path / 'grammars'

    def test_get_cpp_generator(self):
        # Given
        grammar = self.path_grammar / 'grammar_add_mul_with_lexer.bnf'
        entry_rule = 'E'

        # When
        subject = ParserGenerator(grammar, entry_rule=entry_rule)

        # Then
        self.assertEqual(subject.generator.__name__, CppSourceParserGenerator.__name__)

    def test_cpp_generator_get_parsing_table(self):
        # Given
        grammar = self.path_grammar / 'grammar_add_mul_with_lexer.bnf'
        entry_rule = 'E'

        # When
        slr = SLR(grammar, entry_rule)
        slr._add_augmented_entry_point()
        goto, action, start_idx = slr.generate_parsing_table()
        g = CppSourceParserGenerator(goto_table=goto, action_table=action, start_idx=start_idx,
                                     main_directory='unused', include_directory='unused')
        subject_goto, subject_action = g._generate_parsing_table()

        # Then
        self.assertEqual(subject_goto.count('='), 9)
        self.assertEqual(subject_action.count('SLRAccept'), 1)
        self.assertEqual(subject_action.count('SLRShift'), 13)
        self.assertEqual(subject_action.count('SLRReduce'), 22)
