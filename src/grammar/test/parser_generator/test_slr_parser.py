import unittest
import os
from pathlib import Path

from frontend_gen.parser_generator.bnf import *
from frontend_gen.parser_generator.slr_parser import SLR, EndMarker, Shift, Reduce, Accept
from frontend_gen.parser_generator.item_set import ItemSet
from unittest.mock import patch, mock_open


class TestSLR(unittest.TestCase):
    def setUp(self):
        current_path = Path(os.path.dirname(os.path.realpath(__file__)))
        self.path_grammar = current_path / 'grammars'

    def test_first_string_multiple_epsilon(self):
        # Given
        grammar = self.path_grammar / 'grammar_string_epsilon.bnf'
        entry_rule = 'rS'

        expected_rS = [Terminal('A'), Terminal(''), Terminal('a'), Terminal('B'), Terminal('b'), ]

        # When
        slr = SLR(filename_grammar=grammar, entry_rule=entry_rule)

        # Then
        self.assertListEqual(slr.first_string([slr.bnf.symbol_table.get_nonterminal('rS')]), expected_rS)

    def test_first_string_add_mul_polish_grammar(self):
        # Given
        grammar = self.path_grammar / 'grammar_add_mul_polish.bnf'
        entry_rule = 'E'

        expected_E = [Terminal('('), Terminal('a'), Terminal('b'), Terminal('c'), Terminal('x'), Terminal('y'), Terminal('z')]
        expected_E_prime = [Terminal('+'), Terminal('')]
        expected_T = [Terminal('('), Terminal('a'), Terminal('b'), Terminal('c'), Terminal('x'), Terminal('y'), Terminal('z')]
        expected_T_prime = [Terminal('*'), Terminal('')]
        expected_F = [Terminal('('), Terminal('a'), Terminal('b'), Terminal('c'), Terminal('x'), Terminal('y'), Terminal('z')]

        # When
        slr = SLR(filename_grammar=grammar, entry_rule=entry_rule)

        # Then
        self.assertListEqual(slr.first_string([slr.bnf.symbol_table.get_nonterminal('E')]), expected_E)
        self.assertListEqual(slr.first_string([slr.bnf.symbol_table.get_nonterminal('E_prime')]), expected_E_prime)
        self.assertListEqual(slr.first_string([slr.bnf.symbol_table.get_nonterminal('T')]), expected_T)
        self.assertListEqual(slr.first_string([slr.bnf.symbol_table.get_nonterminal('T_prime')]), expected_T_prime)
        self.assertListEqual(slr.first_string([slr.bnf.symbol_table.get_nonterminal('F')]), expected_F)

    def test_first_string_add_mul_grammar(self):
        # Given
        grammar = self.path_grammar / 'grammar_add_mul.bnf'
        entry_rule = 'E'

        expected_E = [Terminal('('), Terminal('a'), Terminal('b'), Terminal('c'), Terminal('x'), Terminal('y'), Terminal('z')]
        expected_T = [Terminal('('), Terminal('a'), Terminal('b'), Terminal('c'), Terminal('x'), Terminal('y'), Terminal('z')]
        expected_F = [Terminal('('), Terminal('a'), Terminal('b'), Terminal('c'), Terminal('x'), Terminal('y'), Terminal('z')]

        # When
        slr = SLR(filename_grammar=grammar, entry_rule=entry_rule)

        # Then
        self.assertListEqual(slr.first_string([slr.bnf.symbol_table.get_nonterminal('E')]), expected_E)
        self.assertListEqual(slr.first_string([slr.bnf.symbol_table.get_nonterminal('T')]), expected_T)
        self.assertListEqual(slr.first_string([slr.bnf.symbol_table.get_nonterminal('F')]), expected_F)

    def test_follow_epsilon_grammar(self):
        # Given
        grammar = self.path_grammar / 'grammar_string_epsilon.bnf'
        entry_rule = 'rS'

        expected = {'rA': [EndMarker(), Terminal('a'), Terminal('B')], 'rB': [Terminal('b'), EndMarker()],
                    'rS': [EndMarker()]}

        # When
        subject = SLR(filename_grammar=grammar, entry_rule=entry_rule)

        # Then
        self.assertCountEqual(subject.follow('rS'), expected['rS'])
        self.assertCountEqual(subject.follow('rA'), expected['rA'])
        self.assertCountEqual(subject.follow('rB'), expected['rB'])

    def test_follow_add_mul_polish_grammar(self):
        # Given
        grammar = self.path_grammar / 'grammar_add_mul_polish.bnf'
        entry_rule = 'E'

        expected = {'E': [EndMarker(), Terminal(')')], 'T': [Terminal('+'), Terminal(')'), EndMarker()],
                    'E_prime': [EndMarker(), Terminal(')')], 'T_prime': [Terminal('+'), EndMarker(), Terminal(')')],
                    'F': [Terminal('+'), EndMarker(), Terminal(')'), Terminal('*')],
                    'id': [Terminal('+'), EndMarker(), Terminal(')'), Terminal('*')]}

        # When
        subject = SLR(filename_grammar=grammar, entry_rule=entry_rule)

        # Then
        self.assertCountEqual(subject.follow('E'), expected['E'])
        self.assertCountEqual(subject.follow('T'), expected['T'])
        self.assertCountEqual(subject.follow('E_prime'), expected['E_prime'])
        self.assertCountEqual(subject.follow('T_prime'), expected['T_prime'])
        self.assertCountEqual(subject.follow('F'), expected['F'])
        self.assertCountEqual(subject.follow('id'), expected['id'])

    def test_follow_add_mul(self):
        # Given
        grammar = self.path_grammar / 'grammar_add_mul.bnf'
        entry_rule = 'E'

        expected = {'E': [EndMarker(), Terminal(')'), Terminal('+')],
                    'T': [Terminal('+'), Terminal(')'), EndMarker(), Terminal('*')],
                    'F': [Terminal('+'), Terminal(')'), EndMarker(), Terminal('*')],
                    'parenthesis_open': [Terminal('('), Terminal('a'), Terminal('b'), Terminal('c'), Terminal('x'), Terminal('y'), Terminal('z')],
                    'parenthesis_close': [Terminal('+'), Terminal(')'), EndMarker(), Terminal('*')],
                    'plus': [Terminal('('), Terminal('a'), Terminal('b'), Terminal('c'), Terminal('x'), Terminal('y'), Terminal('z')],
                    'mul': [Terminal('('), Terminal('a'), Terminal('b'), Terminal('c'), Terminal('x'), Terminal('y'), Terminal('z')],
                    'id': [Terminal('+'), Terminal(')'), EndMarker(), Terminal('*')]}

        # When
        subject = SLR(filename_grammar=grammar, entry_rule=entry_rule)

        # Then
        self.assertCountEqual(subject.follow('E'), expected['E'])
        self.assertCountEqual(subject.follow('T'), expected['T'])
        self.assertCountEqual(subject.follow('F'), expected['F'])
        self.assertCountEqual(subject.follow('parenthesis_open'), expected['parenthesis_open'])
        self.assertCountEqual(subject.follow('parenthesis_close'), expected['parenthesis_close'])
        self.assertCountEqual(subject.follow('plus'), expected['plus'])
        self.assertCountEqual(subject.follow('mul'), expected['mul'])
        self.assertCountEqual(subject.follow('id'), expected['id'])

    def test_closure_from_start_symbol(self):
        # Given
        grammar = self.path_grammar / 'grammar_add_mul_with_lexer.bnf'
        entry_rule = 'E'

        slr = SLR(filename_grammar=grammar, entry_rule=entry_rule)
        slr._add_augmented_entry_point()

        expected = [('E', 0, 0), ('E', 1, 0), ('T', 0, 0), ('T', 1, 0), ('F', 0, 0), ('F', 1, 0),
                    (slr.bnf.start_symbol, 0, 0)]

        # When
        subject = slr.closure(ItemSet([(slr.bnf.start_symbol, 0, 0)]))

        # Then
        self.assertEqual(frozenset(expected), subject)

    def test_closure_non_zero_index(self):
        # Given
        grammar = self.path_grammar / 'grammar_add_mul_with_lexer.bnf'
        entry_rule = 'E'

        slr = SLR(filename_grammar=grammar, entry_rule=entry_rule)
        slr._add_augmented_entry_point()

        expected = [('T', 0, 2), ('F', 0, 0), ('F', 1, 0)]

        # When
        subject = slr.closure(ItemSet([('T', 0, 2)]))

        # Then
        self.assertEqual(frozenset(expected), subject)

    def test_closure_index_at_terminal(self):
        # Given
        grammar = self.path_grammar / 'grammar_add_mul_with_lexer.bnf'
        entry_rule = 'E'

        slr = SLR(filename_grammar=grammar, entry_rule=entry_rule)
        slr._add_augmented_entry_point()

        expected = [(slr.bnf.start_symbol, 0, 1), (slr.bnf.start_symbol, 1, 1)]

        # When
        subject = slr.closure(ItemSet([(slr.bnf.start_symbol, 0, 1), (slr.bnf.start_symbol, 1, 1)]))

        # Then
        self.assertEqual(frozenset(expected), subject)

    def test_goto_from_start_symbol(self):
        # Given
        grammar = self.path_grammar / 'grammar_add_mul_with_lexer.bnf'
        entry_rule = 'E'

        slr = SLR(filename_grammar=grammar, entry_rule=entry_rule)
        slr._add_augmented_entry_point()

        expected = [('E', 0, 2),
                    ('T', 0, 0), ('T', 1, 0),
                    ('F', 0, 0), ('F', 1, 0)]

        items = ItemSet([(slr.bnf.start_symbol, 0, 1), ('E', 0, 1)])
        term = slr.bnf.symbol_table.get_terminal('plus')

        # When
        subject = slr.goto(items, term)

        # Then
        self.assertEqual(frozenset(expected), subject)

    def test_goto_solo_state(self):
        # Given
        grammar = self.path_grammar / 'grammar_add_mul_with_lexer.bnf'
        entry_rule = 'E'

        slr = SLR(filename_grammar=grammar, entry_rule=entry_rule)
        slr._add_augmented_entry_point()

        expected = [('T', 0, 3)]

        items = ItemSet([('T', 0, 2), ('F', 0, 0), ('F', 1, 0)])
        term = slr.bnf.symbol_table.get_nonterminal('F')

        # When
        subject = slr.goto(items, term)

        # Then
        self.assertEqual(frozenset(expected), subject)

    def test_generate_canonical_states(self):
        # Given
        grammar = self.path_grammar / 'grammar_add_mul_with_lexer.bnf'
        entry_rule = 'E'

        slr = SLR(filename_grammar=grammar, entry_rule=entry_rule)
        slr._add_augmented_entry_point()

        expected = [slr.closure(ItemSet([(slr.bnf.start_symbol, 0, 0)])),
                    slr.closure(ItemSet([(slr.bnf.start_symbol, 0, 1), ('E', 0, 1)])),
                    slr.closure(ItemSet([('E', 1, 1), ('T', 0, 1)])),
                    slr.closure(ItemSet([('F', 1, 1)])),
                    slr.closure(ItemSet([('F', 0, 1)])),
                    slr.closure(ItemSet([('T', 1, 1)])),
                    slr.closure(ItemSet([('E', 0, 2)])),
                    slr.closure(ItemSet([('T', 0, 2)])),
                    slr.closure(ItemSet([('E', 0, 1), ('F', 0, 2)])),
                    slr.closure(ItemSet([('E', 0, 3), ('T', 0, 1)])),
                    slr.closure(ItemSet([('T', 0, 3)])),
                    slr.closure(ItemSet([('F', 0, 3)]))
                    ]
        # When
        subject, _ = slr.get_canonical_sets()

        # Then
        self.assertEqual(12, len(subject))
        self.assertCountEqual(expected, subject)

    def test_generate_parsing_table(self):
        # Given
        grammar = self.path_grammar / 'grammar_add_mul_with_lexer.bnf'
        entry_rule = 'E'

        slr = SLR(filename_grammar=grammar, entry_rule=entry_rule)
        slr._add_augmented_entry_point()

        d_translation = {}  # Translation of indices from the manually built parsing table states (key)
                            # to this code generated ones (values).

        expected_goto_manual = {(0, NonTerminal('E')): 1, (0, NonTerminal('T')): 2, (0, NonTerminal('F')): 3,
                                (4, NonTerminal('E')): 8, (4, NonTerminal('T')): 2, (4, NonTerminal('F')): 3,
                                (6, NonTerminal('T')): 9, (6, NonTerminal('F')): 3,
                                (7, NonTerminal('F')): 10}

        terminal_id = slr.bnf.symbol_table.get_terminal('id')
        terminal_parenthesis_o = slr.bnf.symbol_table.get_terminal('parenthesis_open')
        terminal_parenthesis_c = slr.bnf.symbol_table.get_terminal('parenthesis_close')
        terminal_plus = slr.bnf.symbol_table.get_terminal('plus')
        terminal_mul = slr.bnf.symbol_table.get_terminal('mul')

        # When
        subject_goto, subject_action, idx_start = slr.generate_parsing_table()

        # We now need to map to generated table state index, to the one computed manually
        # We can detect most of the mapping through the goto table, however a couple of states
        # cannot be captured by it.

        d_translation[0] = idx_start

        for i in subject_goto:
            l = [(k, subject_goto[k]) for k in subject_goto if k[0] == i[0]]
            manual_idx = None
            if i[0] == idx_start:
                manual_idx = 0
            elif len(l) == 3:
                manual_idx = 4
            elif len(l) == 2:
                manual_idx = 6
            elif len(l) == 1:
                manual_idx = 7

            if manual_idx not in d_translation:
                d_translation[manual_idx] = i[0]
            d_translation[expected_goto_manual[(manual_idx, i[1])]] = subject_goto[i]

        missing_to = list(set(range(0, 12)).difference(d_translation.values()))

        # We are only missing the map from state 5 and 11 (i.e. state from the manual computations)
        # We just need to remap them manually with the right, generated, one
        if (missing_to[0], terminal_plus) in subject_action\
                and subject_action[(missing_to[0], terminal_plus)] == Reduce('F', 1, 1):
            d_translation[5] = missing_to[0]
            d_translation[11] = missing_to[1]
        else:
            d_translation[5] = missing_to[1]
            d_translation[11] = missing_to[0]

        expected_goto = {(d_translation[k[0]], k[1]): d_translation[expected_goto_manual[k]]
                         for k in expected_goto_manual}

        expected_action = {(d_translation[0], terminal_id): Shift(d_translation[5]),
                           (d_translation[0], terminal_parenthesis_o): Shift(d_translation[4]),
                           (d_translation[1], terminal_plus): Shift(d_translation[6]),
                           (d_translation[1], EndMarker()): Accept(),
                           (d_translation[2], terminal_plus): Reduce('E', 1, 1),
                           (d_translation[2], terminal_mul): Shift(d_translation[7]),
                           (d_translation[2], terminal_parenthesis_c): Reduce('E', 1, 1),
                           (d_translation[2], EndMarker()): Reduce('E', 1, 1),
                           (d_translation[3], terminal_plus): Reduce('T', 1, 1),
                           (d_translation[3], terminal_mul): Reduce('T', 1, 1),
                           (d_translation[3], terminal_parenthesis_c): Reduce('T', 1, 1),
                           (d_translation[3], EndMarker()): Reduce('T', 1, 1),
                           (d_translation[4], terminal_id): Shift(d_translation[5]),
                           (d_translation[4], terminal_parenthesis_o): Shift(d_translation[4]),
                           (d_translation[5], terminal_plus): Reduce('F', 1, 1),
                           (d_translation[5], terminal_mul): Reduce('F', 1, 1),
                           (d_translation[5], terminal_parenthesis_c): Reduce('F', 1, 1),
                           (d_translation[5], EndMarker()): Reduce('F', 1, 1),
                           (d_translation[6], terminal_id): Shift(d_translation[5]),
                           (d_translation[6], terminal_parenthesis_o): Shift(d_translation[4]),
                           (d_translation[7], terminal_id): Shift(d_translation[5]),
                           (d_translation[7], terminal_parenthesis_o): Shift(d_translation[4]),
                           (d_translation[8], terminal_plus): Shift(d_translation[6]),
                           (d_translation[8], terminal_parenthesis_c): Shift(d_translation[11]),
                           (d_translation[9], terminal_plus): Reduce('E', 0, 3),
                           (d_translation[9], terminal_mul): Shift(d_translation[7]),
                           (d_translation[9], terminal_parenthesis_c): Reduce('E', 0, 3),
                           (d_translation[9], EndMarker()): Reduce('E', 0, 3),
                           (d_translation[10], terminal_plus): Reduce('T', 0, 3),
                           (d_translation[10], terminal_mul): Reduce('T', 0, 3),
                           (d_translation[10], terminal_parenthesis_c): Reduce('T', 0, 3),
                           (d_translation[10], EndMarker()): Reduce('T', 0, 3),
                           (d_translation[11], terminal_plus): Reduce('F', 0, 3),
                           (d_translation[11], terminal_mul): Reduce('F', 0, 3),
                           (d_translation[11], terminal_parenthesis_c): Reduce('F', 0, 3),
                           (d_translation[11], EndMarker()): Reduce('F', 0, 3)}

        # Then
        self.assertDictEqual(subject_goto, expected_goto)
        self.assertDictEqual(subject_action, expected_action)
