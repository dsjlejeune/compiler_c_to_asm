import unittest

from frontend_gen.parser_generator.bnf import *


class TestRules(unittest.TestCase):

	def test_ConjunctionRule_capture_one_nonterminal(self):

		# Given
		statement = '<nonterminal1>'
		symbol_table = SymbolTable()
		non_terminal_1 = NonTerminal('nonterminal1', [])
		symbol_table.add_nonterminal(non_terminal_1)

		# When
		subject = ConjunctionRule(statement, symbol_table)

		# Then
		self.assertEqual(subject.rules[0], non_terminal_1)

	def test_ConjunctionRule_capture_two_nonterminals(self):

		# Given
		statement = '<nonterminal1> <nonterminal2>'
		symbol_table = SymbolTable()
		non_terminal_1 = NonTerminal('nonterminal1', [])
		non_terminal_2 = NonTerminal('nonterminal2', [])
		symbol_table.add_nonterminal(non_terminal_1)
		symbol_table.add_nonterminal(non_terminal_2)
		expected = [non_terminal_1, non_terminal_2]

		# When
		subject = ConjunctionRule(statement, symbol_table)

		# Then
		self.assertEqual(subject.rules, expected)
	
	def test_ConjunctionRule_capture_one_nonterminal_and_one_terminal(self):

		# Given
		statement = '<nonterminal1> "terminal"'
		symbol_table = SymbolTable()
		non_terminal_1 = NonTerminal('nonterminal1', [])
		terminal = Terminal('terminal')
		symbol_table.add_nonterminal(non_terminal_1)
		symbol_table.add_terminal(terminal)
		expected = [non_terminal_1, terminal]

		# When
		subject = ConjunctionRule(statement, symbol_table)

		# Then
		self.assertEqual(subject.rules, expected)
	
	def test_ConjunctionRule_capture_new_terminal_symbol(self):

		# Given
		statement = '"terminal1" "terminal2"'
		symbol_table = SymbolTable()
		terminal_1 = Terminal('terminal1')
		symbol_table.add_terminal(terminal_1)
		expected = 'terminal2'

		# When
		subject = ConjunctionRule(statement, symbol_table)

		# Then
		self.assertEqual(subject.rules[1].get_symbol(), expected)

	def test_ConjunctionRule_fail_on_undefined_nonterminal(self):

		# Given
		statement = "<undefined_nonterminal>"
		symbol_table = SymbolTable()
		
		# When
		# Then
		with self.assertRaisesRegex(Exception, '.*<undefined_nonterminal> is used but never defined.*'):
			subject = ConjunctionRule(statement, symbol_table)

	def test_SymbolTable_fail_on_already_defined_terminal(self):
		# Given
		symbol_table = SymbolTable()
		terminal_name = 'terminal_name'
		terminal_1 = Terminal(symbol='a', name=terminal_name)
		terminal_1_prime = Terminal(symbol='b', name=terminal_name)
		symbol_table.add_terminal(terminal_1)

		# When
		# Then
		with self.assertRaisesRegex(Exception, f'<{terminal_name}> has already been defined'):
			symbol_table.add_terminal(terminal_1_prime)

	def test_SymbolTable_fail_on_already_defined_nonterminal(self):
		# Given
		symbol_table = SymbolTable()
		nonterminal_name = 'terminal_name'
		nonterminal_1 = NonTerminal(nonterminal_name)
		nonterminal_1_prime = NonTerminal(nonterminal_name)
		symbol_table.add_nonterminal(nonterminal_1)

		# When
		# Then
		with self.assertRaisesRegex(Exception, f'<{nonterminal_name}> has already been defined'):
			symbol_table.add_terminal(nonterminal_1_prime)

	def test_ConjunctionRule_has_only_terminal(self):
		# Given
		statement = '"terminal1" "terminal2"'
		symbol_table = SymbolTable()
		terminal_1 = Terminal('terminal1')
		terminal_2 = Terminal('terminal2')
		symbol_table.add_terminal(terminal_1)
		symbol_table.add_terminal(terminal_2)

		# When
		subject = ConjunctionRule(statement, symbol_table)

		# Then
		self.assertTrue(subject.has_terminal_only())

	def test_ConjunctionRule_has_non_terminal(self):
		# Given
		statement = '"terminal1" <non-terminal1>'
		symbol_table = SymbolTable()
		terminal_1 = Terminal('terminal1')
		nonterminal_1 = NonTerminal('non-terminal1', [])
		symbol_table.add_terminal(terminal_1)
		symbol_table.add_nonterminal(nonterminal_1)

		# When
		subject = ConjunctionRule(statement, symbol_table)

		# Then
		self.assertFalse(subject.has_terminal_only())

if __name__ == '__main__':
	unittest.main()
