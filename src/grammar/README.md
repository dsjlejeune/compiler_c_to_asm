## Compiler's front-end generator

This sub-project aims at providing a way of describing the lexer and parser's grammar such that adding rules for the main C++ compiler project is easy (and iterative).
This doesn't aim at being a fully fledged lexer/parser generator in the sense that it does make a lot of assumption with respect to the necessary code needed to execute the lexer/parser classes. In other words, the generator has been solely designed to meet the needs encounter in building this c-to-asm compiler project.


Some features:

- The lexer rules are defined using regular expressions. The syntax, expressed in Backus–Naur form (BNF), is `'<'<rule_name>'>'<space>::=<space><regex>`. For example, the rule recognizing the identifiers (e.g. variable/function name): `<identifier> ::= [a-zA-Z_$][a-zA-Z0-9_$]*`
- In case of ambiguity in the rule definition, the first defined rule is chosen to detect the token and lexeme when simulating the (non-)deterministic state machine.
- The regex matching is lazy. In other word it matches the smallest possible string given a regular expression. It's useful for expression containing the Kleen-star combined over the 'any' symbol, for instance to match strings, defined as `<string> ::= ".*"`. Without the lazy mode, the lexer would read the rest of file, as anything matches `.*`.
- The parser is a bottom-up SLR

Some caveats:

- The regex language does not allow complement set. For example the syntax `[^ab]` (matching any characters other than 'a' or 'b') is not supported. The 'group' referencing is not supported either (e.g. `(ab)\1`)
- Currently the lexer is modelled using a NFA. For our usage a DFA would be more suitable as it would reduce the number of possible states. Although the conversion NFA -> DFA is already implemented the lexer generator does not (yet) make use of it. Hopefully further iteration will address this.
- The definition of the lexical rules does not allow to specify post-processing, once the token is detected. For instance Lex and ANTLR allow it. For the time being, this post-processing is done 'manually' when necessary on the C++ side after the lexer has been generated. For example, this post-processing would allow to convert a string representing numbers into number type: `"123"` -> `(int) 123`
- Sometimes it's necessary to specify the need of a lookahead in the lexer's rules definition. This is not supported here.
- The definition of a terminal symbol (lexer) cannot be used in another definition. For instance if digits is defined by `<digit> ::= [0-9]` it cannot be used in the definition of numerics. The following is not allowed: `<numeric> ::= <digit>+`.
- But, the definition of a non-terminal symbol in the context-free grammar file (parser) *can* be used in another rule (including itself). 

### Generate the front-end

The project is fully implemented in Python3. There are no other requirements or dependencies.

Running (while located in this directory):

    $ python3 generating_frontend.py

will generate (and overwrite!) the files in the C++ src/include folders. The generation of the lexer modifies files in `src/lexer/lexer.cpp` and `include/lexer/lexer.hpp`. The generation of the parser modifies the files `src/parser/parser.cpp` and `include/parser/parser.hpp` 

### Running the unit tests suit

While located in this directory, run:

    $ python3 -m unittest -v

Even though there are some level of test in this part of the project, more tests are done on the generated codes in the C++ side. For example see `test/lexer/lexer_generated_test.cpp`

### Architecture of the sub-project

- Data structures
    - `./frontend_gen/tree/*` : implementation of n-ary tree.
    - `./frontend_gen/finite_automaton/*`: implementation of NFA (building the automaton, simulating it and converting it to DFA), DFA and states' definition associated to these automatons.
- Lexer generator classes. The architecture of the generator is pretty linear: read raw file containing the rules -> convert these rules into parse trees -> convert this set of parse trees into one NFA -> write the NFA into C++ format to form the C++ lexer. In details:
    - `./frontend_gen/lexer_generator/read_lex_rules.py`: reads the raw file containing the lexical rules of the form `<rule> ::= abc*`
    - `./frontend_gen/lexer_generator/regex_parser.py`: consumes the output of `read_lex_rules.py`. It's used to parse a regex rule and build a parse tree out of it. That's where the meat is.
    - `./frontend_gen/lexer_generator/regex_to_nfa.py`: consumes the regexes' parse tree and transform it into one aggregated NFA
    - `./frontend_gen/lexer_generator.py`: writes the NFA into C++ following the templates defined in the `generator/cpp` folder.
    - `./lexer.reg`: contains the current set of lexical rules. This is the file to update to change the terminal symbols recognized by the language we want to compile.
- Parser generator classes:
    - `./frontend_gen/parser_generator/bnf.py`: does the heavy lifting of reading the Backus–Naur form grammar file and importing the lexemes from the lexer `*.reg` file. It parses the bnf using a simple push-down automaton and transforms the rules into workable symbols table and conjunction/disjunction rules set.
    - `./frontend_gen/parser_generator/slr_parser.py`: implements the necessary ingredients to build the SLR(1) parsing table.
    - `./frontend_gen/parser_generator/parser_generator.py`: writes the parsing table into C++ following the templates defined in the `generator/cpp` folder
    - `./c_grammar.bnf`: contains the syntactical rules specified in a simplified Backus–Naur form which define the syntax of the language to compile. It's possible to refer to the lexical rules by using the `import` keyword used to refer to a `*.reg` file.
- Main entry point:
    - `./generating_frontend.py`: generates the C++ lexer for the terminal symbols defined in `lexer.reg` and generates the C++ parser for the non-terminal symbols defined in `c_grammar.bnf`
