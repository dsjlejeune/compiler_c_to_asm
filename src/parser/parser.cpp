/*
 * FILE GENERATED AUTOMATICALLY BY THE PARSER GENERATOR
 * DON'T MODIFY!
 * ALL MANUAL CHANGES ARE AT RISK TO BE OVERWRITTEN!
*/
#include "parser/parser.hpp"
#include "parser/parser_actions.hpp"

Parser::Parser(const Lexer& lexer): ParserBase(lexer) {
    this->buildParsingTable();
    this->init_stack(0);
}

void Parser::buildParsingTable() {

	// GOTO table
	this->table_goto[std::pair<int, std::string>(0, "T")] = 2;
	this->table_goto[std::pair<int, std::string>(0, "E")] = 3;
	this->table_goto[std::pair<int, std::string>(0, "F")] = 4;
	this->table_goto[std::pair<int, std::string>(5, "T")] = 2;
	this->table_goto[std::pair<int, std::string>(5, "E")] = 8;
	this->table_goto[std::pair<int, std::string>(5, "F")] = 4;
	this->table_goto[std::pair<int, std::string>(6, "F")] = 9;
	this->table_goto[std::pair<int, std::string>(7, "T")] = 10;
	this->table_goto[std::pair<int, std::string>(7, "F")] = 4;

	// Action table
	this->table_action[std::pair<int, std::string>(0, "identifier")] = new SLRShift(1);
	this->table_action[std::pair<int, std::string>(0, "parenthesis_open")] = new SLRShift(5);
	this->table_action[std::pair<int, std::string>(1, "plus")] = new SLRReduce("F", 1, 1);
	this->table_action[std::pair<int, std::string>(1, "parenthesis_close")] = new SLRReduce("F", 1, 1);
	this->table_action[std::pair<int, std::string>(1, "$_endmarker")] = new SLRReduce("F", 1, 1);
	this->table_action[std::pair<int, std::string>(1, "star")] = new SLRReduce("F", 1, 1);
	this->table_action[std::pair<int, std::string>(2, "star")] = new SLRShift(6);
	this->table_action[std::pair<int, std::string>(2, "plus")] = new SLRReduce("E", 1, 1);
	this->table_action[std::pair<int, std::string>(2, "parenthesis_close")] = new SLRReduce("E", 1, 1);
	this->table_action[std::pair<int, std::string>(2, "$_endmarker")] = new SLRReduce("E", 1, 1);
	this->table_action[std::pair<int, std::string>(3, "plus")] = new SLRShift(7);
	this->table_action[std::pair<int, std::string>(3, "$_endmarker")] = new SLRAccept();
	this->table_action[std::pair<int, std::string>(4, "plus")] = new SLRReduce("T", 1, 1);
	this->table_action[std::pair<int, std::string>(4, "parenthesis_close")] = new SLRReduce("T", 1, 1);
	this->table_action[std::pair<int, std::string>(4, "$_endmarker")] = new SLRReduce("T", 1, 1);
	this->table_action[std::pair<int, std::string>(4, "star")] = new SLRReduce("T", 1, 1);
	this->table_action[std::pair<int, std::string>(5, "identifier")] = new SLRShift(1);
	this->table_action[std::pair<int, std::string>(5, "parenthesis_open")] = new SLRShift(5);
	this->table_action[std::pair<int, std::string>(6, "identifier")] = new SLRShift(1);
	this->table_action[std::pair<int, std::string>(6, "parenthesis_open")] = new SLRShift(5);
	this->table_action[std::pair<int, std::string>(7, "identifier")] = new SLRShift(1);
	this->table_action[std::pair<int, std::string>(7, "parenthesis_open")] = new SLRShift(5);
	this->table_action[std::pair<int, std::string>(8, "plus")] = new SLRShift(7);
	this->table_action[std::pair<int, std::string>(8, "parenthesis_close")] = new SLRShift(11);
	this->table_action[std::pair<int, std::string>(9, "plus")] = new SLRReduce("T", 0, 3);
	this->table_action[std::pair<int, std::string>(9, "parenthesis_close")] = new SLRReduce("T", 0, 3);
	this->table_action[std::pair<int, std::string>(9, "$_endmarker")] = new SLRReduce("T", 0, 3);
	this->table_action[std::pair<int, std::string>(9, "star")] = new SLRReduce("T", 0, 3);
	this->table_action[std::pair<int, std::string>(10, "plus")] = new SLRReduce("E", 0, 3);
	this->table_action[std::pair<int, std::string>(10, "parenthesis_close")] = new SLRReduce("E", 0, 3);
	this->table_action[std::pair<int, std::string>(10, "$_endmarker")] = new SLRReduce("E", 0, 3);
	this->table_action[std::pair<int, std::string>(10, "star")] = new SLRShift(6);
	this->table_action[std::pair<int, std::string>(11, "plus")] = new SLRReduce("F", 0, 3);
	this->table_action[std::pair<int, std::string>(11, "parenthesis_close")] = new SLRReduce("F", 0, 3);
	this->table_action[std::pair<int, std::string>(11, "$_endmarker")] = new SLRReduce("F", 0, 3);
	this->table_action[std::pair<int, std::string>(11, "star")] = new SLRReduce("F", 0, 3);
}