cmake_minimum_required (VERSION 3.8)
project (compiler_c_to_asm)

file(GLOB LIB_SRC
	"finite_automaton/*.cpp" 
	"lexer/*.cpp"
	"parser/*.cpp"
	"utils/*.cpp"
)

add_library (lib_static ${LIB_SRC})
#add_library (lib_shared SHARED ${LIB_SRC})

add_executable (compiler compiler.cpp ${LIB_SRC})
#target_link_libraries (compiler lib_static)
