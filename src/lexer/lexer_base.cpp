#include<filesystem>
#include<string>
#include<fstream>
#include<sstream>
#include<vector>

#include "lexer/lexer_base.hpp"
#include "utils/file_reader.hpp"

namespace fs = std::filesystem;

LexerBase::LexerBase(const fs::path& filename) noexcept(false) {
	// TODO: implement a better stream consumption
	// with dynamic loading (and freeeing) of small buffers
	// -> Need to implement a StreamBufferManager
	// But for now, and for simplicity, we will load
	// the whole source code in-memory
	FileReader fr{filename};

	this->source_code = fr.read();

	fr.close();

	this->stream_position = 0;
	this->lookahead = 0;
}

LexerBase::~LexerBase() {
	if (this->nfa != nullptr) {
		delete this->nfa;
	}
}


void LexerBase::buildNFA() {
}


int LexerBase::getIndexOfFirstAcceptingState(const std::vector<FAState<std::string, std::string>>& states){
	for(int i=0; i<states.size();i++) {
		if(states[i].isAccepting()) {
			return i;
		}
	}
	return -1;
}

const std::vector<FAState<std::string, std::string>>* LexerBase::getAllAcceptingStates(const std::vector<FAState<std::string, std::string>>& states) {
	std::vector<FAState<std::string, std::string>>* ret = new std::vector<FAState<std::string, std::string>>();
	for(auto elem: states) {
		if(elem.isAccepting()) {
			ret->push_back(elem);
		}
	}
	return ret;
}

bool LexerBase::hasSameRule(const std::vector<FAState<std::string, std::string>>& states) {
	bool ret = true;
	std::string rule_name;
	if(states.size() > 0) {
		rule_name = states[0].getRuleName();
	}
	for(const auto elem: states) {
		if(elem.getRuleName() != rule_name) {
			return false;
		}
	}
	return ret;
}

std::string print_states(const std::vector<FAState<std::string, std::string>>* states) {
	std::stringstream ss;
	for(auto s:*states) {
		ss << s.getRuleName() << "-" << s.getId();

        if(s.isAccepting())
			ss << "(Accepting)";
			
		ss << ", ";
	}

	return ss.str();
}

const token_t* LexerBase::nextToken() {
	int sentinel = this->stream_position;
	const std::vector<FAState<std::string, std::string>>* current_states = new std::vector<FAState<std::string, std::string>>({this->nfa->getInitState()});
	std::stringstream ss;

	while (sentinel < this->source_code.length()) {
		std::string current_char;

		// Handling escape characters
		if(this->source_code[sentinel] == '\\' && sentinel+1 < this->source_code.length()) {
			current_char = std::string(&(this->source_code[sentinel]), 2);
			sentinel++;
		} else {
			current_char = std::string(1, this->source_code[sentinel]);
		}

		auto* eps_closure = this->nfa->epsilon_closure(*current_states);
		auto* next_states = this->nfa->move(*eps_closure, current_char);

		if(next_states->size() == 0) {
			int current_index_accepting = this->getIndexOfFirstAcceptingState(*eps_closure);
			if(current_index_accepting < 0) { // Blocked with no accepting state
				//TODO: throw an error: unrecognized token
				
				delete current_states;
				delete next_states;
				delete eps_closure;

				return nullptr;
			} else {
				token_t* ret = new token_t({(*eps_closure)[current_index_accepting].getRuleName(), ss.str()});
				this->stream_position = sentinel;

				delete current_states;
				delete next_states;
				delete eps_closure;

				return ret;
			}
		} else if(this->hasSameRule(*next_states)) { // Handle longest prefix but prefer accepting states over "ANY chars" when possible (i.e. opportunist behavior)
			auto* tmp_accepting_states = this->getAllAcceptingStates(*next_states);
			if(tmp_accepting_states->size() > 0) {
				delete next_states;
				next_states = tmp_accepting_states;
			} else {
				delete tmp_accepting_states;
			}
		}
		
		ss << current_char;
		sentinel++;

		delete current_states;
		delete eps_closure;
		current_states = next_states;
	}

	return nullptr;
}

const token_t* LexerBase::nextNonEmptyToken() {
	const token_t* t = this->nextToken();
	bool skip = false;

	do {
		if(t!= nullptr && (t->token_id == "white_space" || t->token_id == "block_comment" || t->token_id == "line_comment")) {
			skip = true;
			delete t;
			t = this->nextToken();
		} else {
			skip = false;
		}
	}while(t != nullptr && skip==true);

	return t;
}

