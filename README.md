## Compiler C to x86 ASM

It's a work in progress implementation of a compiler from a subset of C to x86 ASM. The project is composed of two big parts:

- the front-end generator: it's written in Python. Its goal is to generate the lexer and parser in C++17 based on regexes and BNF descriptions of the input language (i.e. subset of C). More information about that part in the [subproject folder](./src/grammar/)
- the rest of the compiler (the backend):
    - WIP

### Build

This project used CMake as the build system. It's  therefore necessary to have it installed before proceeding. Other external dependencies (e.g. GoogleTest) will downloaded automatically. If not done, generate the building environment (from this README's location):

    $ mkdir build
    $ cd build
    $ cmake ..

To build the project (once inside `./build`):

    $ make

### Running unit tests

Once inside `./build`, running `make test` or `ctest --verbose` will run the test
