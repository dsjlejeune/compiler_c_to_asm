#include<string>
#include "gtest/gtest.h"
#include "gmock/gmock.h"

#include "utils/node.hpp"
#include "utils/tree.hpp"


template<class T> class TreeMockUpFreeNode: public Tree<T> {

	public:
	TreeMockUpFreeNode(const T& root_payload) { this->set_root(root_payload); }
    ~TreeMockUpFreeNode() { }

	void test_free_memory() { Tree<T>::free_memory(); } // Bring the method back from protected to public for testing purposes

	MOCK_METHOD(void, free_node, (Node<T>** node), (override));

};
TreeMockUpFreeNode(const char*) -> TreeMockUpFreeNode<std::string>;

template<class T> class TreeTestBFS: public Tree<T> {

	public:
	TreeTestBFS(T payload): Tree<T>(payload) {}
	std::vector<T> order_bfs;
	
	void test_bfs() { 
		this->bfs_op(&this->root, 
						 std::bind(&TreeTestBFS<T>::op, this, std::placeholders::_1)
						); 
	}
   	void op(Node<T>** node) { order_bfs.push_back((*node)->get_payload()); } 
	
};

TreeTestBFS(const char*) -> TreeTestBFS<std::string>;

TEST(test_tree, adding_nodes) {
	
	// GIVEN 
	auto root_payload = "root";
	auto left_payload = "left"; 
	auto right_payload = "right";


	// WHEN
	auto subject = new Tree(root_payload);
	subject->add_child_with_payload(subject->get_root(), left_payload);
	subject->add_child_with_payload(subject->get_root(), right_payload);

	// THEN
    EXPECT_EQ(subject->get_root()->get_children().size(), 2);
	EXPECT_EQ(subject->get_root()->get_child_at(0)->get_payload(), left_payload);
	EXPECT_EQ(subject->get_root()->get_child_at(1)->get_payload(), right_payload);

	delete subject;
}

TEST(test_tree, clean_up_memory) {
	
	// GIVEN 
	auto root_payload = "root";
	auto left_payload = "left"; 
	auto right_payload = "right";


	// WHEN, THEN
	auto subject_tree = new TreeMockUpFreeNode(root_payload);
	auto subject_node_root = subject_tree->get_root();
	auto subject_node_left = subject_tree->add_child_with_payload(subject_node_root, left_payload);
	auto subject_node_right = subject_tree->add_child_with_payload(subject_node_root, right_payload);

	EXPECT_CALL(*subject_tree, free_node).Times(3);
	subject_tree->test_free_memory();

    delete subject_tree;
}

TEST(test_tree, bnf_op) {

	// GIVEN
	auto root_payload = "root";
	auto left_payload = "left"; 
	auto right_payload = "right";
	auto tree = new TreeTestBFS(root_payload);
	auto root_node = tree->get_root();
	
	tree->add_child_with_payload(root_node, left_payload);
	tree->add_child_with_payload(root_node, right_payload);

	// WHEN
	tree->test_bfs();
	auto subject = tree->order_bfs;
	
	// THEN
    EXPECT_EQ(subject.size(), 3);
    EXPECT_EQ(subject[0], left_payload);
    EXPECT_EQ(subject[1], right_payload);
    EXPECT_EQ(subject[2], root_payload);
	
	delete tree;
}


