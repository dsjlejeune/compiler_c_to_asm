#include<filesystem>
#include<cstdlib>
#include<iostream>
#include "gtest/gtest.h"
#include "utils/file_reader.hpp"

namespace fs = std::filesystem;

class FileReaderTestFixture : public ::testing::Test {

	protected:
    	std::string data_folder;

		FileReaderTestFixture() {
			data_folder = getEnvVar("DATADIR");
		}
		
		virtual ~FileReaderTestFixture() {
		
		}
		
		virtual void SetUp() {
		
		}
		
		const std::string getEnvVar(const std::string& key) {

   			 char* val = getenv(key.c_str());

		    return val == NULL ? std::string("") : std::string(val);
		}
};

TEST_F(FileReaderTestFixture, open_existing_file) {

	// GIVEN
	const fs::path& filename(data_folder+"file_reader_hello.txt");

	// WHEN
	auto subject = FileReader(filename);

	// THEN
    //EXPECT_EQ(subject.get_payload(), payload);
	SUCCEED();
}

TEST_F(FileReaderTestFixture, open_non_existing_file) {

	// GIVEN
	fs::path filename = data_folder+"file_reader_dont_exist.txt";

	// WHEN
	try {
		// WHEN
		auto subject = FileReader(filename);
		FAIL() << "Expected FileDoesNotExistException to be triggered";
	} catch(const FileDoesNotExistException& error) {
		// THEN
		SUCCEED();
	}  catch(const std::exception& e) {
		FAIL() << "Wrong exception triggered";
	}

}

TEST_F(FileReaderTestFixture, read_file) {

	// GIVEN
	const fs::path& filename(data_folder+"file_reader_hello.txt");
	auto file_reader = FileReader(filename);
	auto file_content = "hello\nworld\n.\n";

	// WHEN
	auto subject = file_reader.read();

	// THEN
    EXPECT_EQ(subject, file_content );
}
