#include "gtest/gtest.h"
#include "utils/node.hpp"

TEST(test_node, create_node) {

	// GIVEN
	auto payload("payload");


	// WHEN
	Node subject(payload);

	// THEN
    EXPECT_EQ(subject.get_payload(), payload);
}

TEST(test_node, add_children) {

	// GIVEN
	auto payload_1("payload_1");
	auto payload_2("payload_2");
    auto [node_1, node_2] = std::pair(new Node(payload_1), new Node(payload_2));
	

	// WHEN
	Node subject("root payload");
	subject.add_child(node_1);
	subject.add_child(node_2);

	// THEN
    EXPECT_EQ(subject.get_children().size(), 2);
    EXPECT_EQ(subject.get_child_at(0)->get_payload(), payload_1);
    EXPECT_EQ(subject.get_child_at(1)->get_payload(), payload_2);

	delete node_1, node_2;
}

TEST(test_node, add_parent) {
	// GIVEN
	auto payload_parent("payload_parent");
	auto payload_child("payload_child");
    //auto [node_parent, node_child] = std::pair(new Node(payload_parent), new Node(payload_child));
	auto parent_node = new Node(payload_parent);
	

	// WHEN
	Node subject(payload_child);
	parent_node->add_child(&subject);

	// THEN
    EXPECT_EQ(subject.get_parent(), parent_node);

	delete parent_node;
}
