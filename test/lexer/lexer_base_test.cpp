#include<filesystem>
#include<cstdlib>
#include "gtest/gtest.h"
#include "lexer/lexer_base.hpp"

namespace fs = std::filesystem;
                      
class LexerBaseToTest: public LexerBase {
	public:
		LexerBaseToTest(const fs::path& path): LexerBase(path) {};
	std::string getSourceCodeContent() {
		return this->source_code;
	}
};


class LexerBaseTestFixture : public ::testing::Test {

	protected:
    	std::string data_folder;

		LexerBaseTestFixture() {
			data_folder = getEnvVar("DATADIR");
		}
		
		virtual ~LexerBaseTestFixture() {
		
		}
		
		virtual void SetUp() {
		
		}
		
		const std::string getEnvVar(const std::string& key) {
			char* val = getenv(key.c_str());
			return val == NULL ? std::string("") : std::string(val);
		}
};

TEST_F(LexerBaseTestFixture, load_file_content) {

	// GIVEN
	auto lexer = LexerBaseToTest(data_folder+"source_code_1.c");
	std::string expected = "#include <stdio.h>\n\nint main() {\n\treturn 0;\n}\n";


	// WHEN
	std::string subject = lexer.getSourceCodeContent();

	// THEN
    EXPECT_EQ(subject, expected);
}

