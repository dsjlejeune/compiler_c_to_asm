#include<filesystem>
#include<cstdlib>
#include "gtest/gtest.h"
#include "lexer/lexer_base.hpp"
#include "lexer/lexer.hpp"

namespace fs = std::filesystem;
                      
class LexerToTest: public Lexer {
	public:
		LexerToTest(const fs::path& path): Lexer(path) {};
	std::string getSourceCodeContent() {
		return this->source_code;
	}
};


class LexerTestFixture : public ::testing::Test {

	protected:
    	std::string data_folder;

		LexerTestFixture() {
			data_folder = getEnvVar("DATADIR");
		}
		
		virtual ~LexerTestFixture() {
		
		}
		
		virtual void SetUp() {
		
		}
		
		const std::string getEnvVar(const std::string& key) {
			char* val = getenv(key.c_str());
			return val == NULL ? std::string("") : std::string(val);
		}
};

TEST_F(LexerTestFixture, wrong_increment) {

	// GIVEN
	auto lexer = LexerToTest(data_folder+"wrong_increment.c");	
	const token_t* token = nullptr; 
	std::vector<token_t> expected {{"identifier", "a"}, {"increment", "++"}, {"plus", "+"}, {"semicolumn", ";"}};
	
	int i=0;

	do {
		if(token != nullptr) {
			delete token;
		}

		//WHEN
 		token = lexer.nextToken();

		// THEN
		if(token != nullptr) {
			EXPECT_EQ(token->token_id, expected[i].token_id);
			EXPECT_EQ(token->lexeme, expected[i].lexeme);
		}

		i++;
			
	} while(token != nullptr);

	EXPECT_EQ(i-1, expected.size());
}

TEST_F(LexerTestFixture, addition_increment) {

	// GIVEN
	auto lexer = LexerToTest(data_folder+"addition_increment.c");	
	const token_t* token = nullptr;
	std::vector<token_t> expected {{"identifier", "a"}, {"increment", "++"}, {"plus", "+"}, {"increment", "++"}, {"identifier", "b"}, {"semicolumn", ";"}};
	int i=0;

	do {
		if(token != nullptr) {
			delete token;
		}

		//WHEN
 		token = lexer.nextNonEmptyToken();

		// THEN
		if(token != nullptr) {
			EXPECT_EQ(token->token_id, expected[i].token_id);
			EXPECT_EQ(token->lexeme, expected[i].lexeme);
		}

		i++;
			
	} while(token != nullptr);
	
	EXPECT_EQ(i-1, expected.size());
}

TEST_F(LexerTestFixture, addition_increment_nospace) {

	// GIVEN
	auto lexer = LexerToTest(data_folder+"addition_increment_nospace.c");	
	const token_t* token = nullptr;
	std::vector<token_t> expected {{"identifier", "a"}, {"increment", "++"}, {"increment", "++"}, {"plus", "+"}, {"identifier", "b"}, {"semicolumn", ";"}};
	int i=0;

	do {
		if(token != nullptr) {
			delete token;
		}

		//WHEN
 		token = lexer.nextNonEmptyToken();

		// THEN
		if(token != nullptr) {
			EXPECT_EQ(token->token_id, expected[i].token_id);
			EXPECT_EQ(token->lexeme, expected[i].lexeme);
		}

		i++;
			
	} while(token != nullptr);
	
	EXPECT_EQ(i-1, expected.size());
}

TEST_F(LexerTestFixture, simple_main) {

	// GIVEN
	auto lexer = LexerToTest(data_folder+"simple_main.c");	
	const token_t* token = nullptr;
	std::vector<token_t> expected {{"type_int", "int"}, {"white_space", " "}, {"identifier", "main"}, {"parenthesis_open", "("},
									{"parenthesis_close", ")"}, {"white_space", " "}, {"curly_brace_open", "{"}, {"white_space", "\n\t"}, {"return", "return"}, 
									{"white_space", " "}, {"numeric", "0"}, {"semicolumn", ";"}, {"white_space", "\n"},	{"curly_brace_close", "}"}};

	int i=0;

	do {
		if(token != nullptr) {
			delete token;
		}

		//WHEN
 		token = lexer.nextToken();

		// THEN
		if(token != nullptr) {
			EXPECT_EQ(token->token_id, expected[i].token_id);
			EXPECT_EQ(token->lexeme, expected[i].lexeme);
		}

		i++;
			
	} while(token != nullptr);
	
	EXPECT_EQ(i-1, expected.size());
}

TEST_F(LexerTestFixture, simple_main_with_comments) {

	// GIVEN
	auto lexer = LexerToTest(data_folder+"simple_main_with_comments.c");	
	const token_t* token = nullptr;
	std::vector<token_t> expected {{"block_comment", "/*\n * block\n*/"}, {"white_space", "\n"}, {"type_int", "int"}, {"white_space", " "}, {"identifier", "main"}, 
									{"parenthesis_open", "("},{"parenthesis_close", ")"}, {"white_space", " "}, {"curly_brace_open", "{"}, {"white_space", "\n\t"}, 
									{"line_comment", "// comment above\n"}, {"white_space", "\t"}, {"return", "return"}, {"white_space", " "}, {"numeric", "0"}, 
									{"semicolumn", ";"}, {"white_space", " "}, {"line_comment", "// in line\n"}, {"curly_brace_close", "}"}};

	int i=0;

	do {
		if(token != nullptr) {
			delete token;
		}

		//WHEN
 		token = lexer.nextToken();

		// THEN
		if(token != nullptr) {
			EXPECT_EQ(token->token_id, expected[i].token_id);
			EXPECT_EQ(token->lexeme, expected[i].lexeme);
		}

		i++;
			
	} while(token != nullptr);
	
	EXPECT_EQ(i-1, expected.size());
}

TEST_F(LexerTestFixture, hello_world) {

	// GIVEN
	auto lexer = LexerToTest(data_folder+"hello_world_printf_args.c");	
	const token_t* token = nullptr;
	std::vector<token_t> expected {{"hash","#"},{"include", "include"}, {"include_lib", "<stdio.h>"}, {"type_int", "int"}, {"identifier", "main"}, 
									{"parenthesis_open", "("},{"type_void", "void"}, {"parenthesis_close", ")"}, {"curly_brace_open", "{"}, {"type_char", "char"},
									{"star", "*"}, {"identifier", "s"}, {"equal", "="}, {"string", "\"hello world!\""}, {"semicolumn", ";"}, {"identifier", "printf"}, 
									{"parenthesis_open", "("}, {"string", "\"%s\\n\""}, {"comma", ","}, {"identifier", "s"}, {"parenthesis_close", ")"}, 
									{"semicolumn", ";"}, {"return", "return"}, {"numeric", "0"}, {"semicolumn", ";"}, {"curly_brace_close", "}"}};

	int i=0;

	do {
		if(token != nullptr) {
			delete token;
		}

		//WHEN
 		token = lexer.nextNonEmptyToken();

		// THEN
		if(token != nullptr) {
			EXPECT_EQ(token->token_id, expected[i].token_id);
			EXPECT_EQ(token->lexeme, expected[i].lexeme);
		}

		i++;
			
	} while(token != nullptr);
	
	EXPECT_EQ(i-1, expected.size());
}

TEST_F(LexerTestFixture, escape_characters_in_string) {

	// GIVEN
	auto lexer = LexerToTest(data_folder+"escape_char.c");	
	const token_t* token = nullptr;
	std::vector<token_t> expected {{"type_char", "char"}, {"star", "*"}, {"identifier", "x"}, {"equal", "="}, {"string", "\"\\\"\\t\\n\""}, {"semicolumn", ";"}};
	int i=0;

	do {
		if(token != nullptr) {
			delete token;
		}

		//WHEN
 		token = lexer.nextNonEmptyToken();

		// THEN
		if(token != nullptr) {
			EXPECT_EQ(token->token_id, expected[i].token_id);
			EXPECT_EQ(token->lexeme, expected[i].lexeme);
		}

		i++;
			
	} while(token != nullptr);
	
	EXPECT_EQ(i-1, expected.size());
}
