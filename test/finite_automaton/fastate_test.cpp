#include <string>

#include "gtest/gtest.h"
#include "finite_automaton/fastate.hpp"

TEST(test_fstate, test_instance) {

	// GIVEN
	int id=1;
	std::string rule_name = "test_rule";
	std::string payload = "some payload";

	// WHEN
	auto state = FAState<int, std::string>(id, rule_name, payload);
	auto subject_id = state.getId();
	auto subject_rule_name = state.getRuleName();
	auto subject_payload = state.getPayload();

	// THEN
	EXPECT_EQ(subject_id, id);
	EXPECT_EQ(subject_rule_name, rule_name);
	EXPECT_EQ(subject_payload, payload);
}

TEST(test_fstate, test_not_accepting_state_by_default) {

	// GIVEN
	// WHEN
	auto subject = FAState<int, std::string>(1, "rule", "payload");

	// THEN
    EXPECT_FALSE(subject.isAccepting());
}

TEST(test_fstate, test_accepting_state) {

	// GIVEN
	auto subject = FAState<int, std::string>(1, "rule", "payload");

	// WHEN
	subject.setAccepting(true);

	// THEN
	EXPECT_TRUE(subject.isAccepting());
}

TEST(test_fstate, test_greater_than_operator) {

	// GIVEN
	auto s1 = FAState<int, std::string>(1, "rule", "payload");
	auto s2 = FAState<int, std::string>(2, "rule", "payload");

	// WHEN
	bool subject = s1 < s2;

	// THEN
	EXPECT_TRUE(subject);
}
