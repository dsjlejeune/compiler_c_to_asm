#include<string>
#include<map>
#include<vector>
#include<iostream>

#include "gtest/gtest.h"
#include "finite_automaton/fastate.hpp"
#include "finite_automaton/nfa.hpp"

template<class T> class NFAMockUp: public NFA<T> {

	public:
	using NFA<T>::NFA;

	std::map<std::pair<FAState<T, std::string>, std::string>, std::vector<FAState<T, std::string>>*>*
		 getTransitionMatrix() { return &(this->transition_matrix); }; // Bring the class-member back from protected to public for testing purposes

};

TEST(test_nfa, adding_transitions) {
	// GIVEN
	std::string rule_name = "test_rule";
	auto init_state = FAState<std::string, std::string>("init_state", rule_name);
	auto s2 = FAState<std::string, std::string>("s2", rule_name);
	auto s3 = FAState<std::string, std::string>("s3", rule_name, "payload", true);
	auto s4 = FAState<std::string, std::string>("s4", rule_name, "payload", true);

	// WHEN
	NFAMockUp<std::string> nfa = NFAMockUp<std::string>(init_state);
	nfa.addTransition(init_state, s2, "a");
	nfa.addTransition(s2, s3, "b");
	nfa.addTransition(init_state, s4, "c");

	auto subject = nfa.getTransitionMatrix();

	// THEN
	EXPECT_EQ(((*subject)[std::pair<FAState<std::string, std::string>, std::string>(init_state, "a")]->size()), 1);
	EXPECT_EQ(((*subject)[std::pair<FAState<std::string, std::string>, std::string>(init_state, "c")]->size()), 1);
	EXPECT_EQ(((*subject)[std::pair<FAState<std::string, std::string>, std::string>(s2, "b")]->size()), 1);

	EXPECT_EQ(((*subject)[std::pair<FAState<std::string, std::string>, std::string>(init_state, "a")]->at(0).getId()), "s2");
	EXPECT_EQ(((*subject)[std::pair<FAState<std::string, std::string>, std::string>(init_state, "c")]->at(0).getId()), "s4");
	EXPECT_EQ(((*subject)[std::pair<FAState<std::string, std::string>, std::string>(s2, "b")]->at(0).getId()), "s3");
}

TEST(test_nfa, epsilon_closure) {
	// GIVEN
	std::string rule_name = "test_rule";
	auto init_state = FAState<std::string, std::string>("init_state", rule_name);
	auto s2 = FAState<std::string, std::string>("s2", rule_name);
	auto s3 = FAState<std::string, std::string>("s3", rule_name, "payload", true);
	auto s4 = FAState<std::string, std::string>("s4", rule_name);
	auto s5 = FAState<std::string, std::string>("s5", rule_name, "payload", true);

	std::vector<FAState<std::string, std::string>> from_state_1 {init_state};
	std::vector<FAState<std::string, std::string>> from_state_2 {s5};

	// WHEN
	// language: b|a+(b)?
	NFA<std::string> subject = NFA<std::string>(init_state);
	subject.addTransition(init_state, s2, EPSILON);
	subject.addTransition(s2, s3, "b");
	subject.addTransition(init_state, s4, EPSILON);
	subject.addTransition(s4, s5, "a");
	subject.addTransition(s5, init_state, EPSILON);

	// THEN    
	EXPECT_EQ((subject.epsilon_closure(from_state_1))->size(), 3);
	EXPECT_EQ((*(subject.epsilon_closure(from_state_1)))[0].getId(), "init_state");
	EXPECT_EQ((*(subject.epsilon_closure(from_state_1)))[1].getId(), "s2");
	EXPECT_EQ((*(subject.epsilon_closure(from_state_1)))[2].getId(), "s4");
	
	EXPECT_EQ((subject.epsilon_closure(from_state_2))->size(), 4);
	EXPECT_EQ((*(subject.epsilon_closure(from_state_2)))[0].getId(), "s5");
	EXPECT_EQ((*(subject.epsilon_closure(from_state_2)))[1].getId(), "init_state");
	EXPECT_EQ((*(subject.epsilon_closure(from_state_2)))[2].getId(), "s2");
	EXPECT_EQ((*(subject.epsilon_closure(from_state_2)))[3].getId(), "s4");
}

TEST(test_nfa, move_simple) {
	// GIVEN
	std::string rule_name = "test_rule";
	auto init_state = FAState<std::string, std::string>("init_state", rule_name);
	auto s2 = FAState<std::string, std::string>("s2", rule_name);
	auto s3 = FAState<std::string, std::string>("s3", rule_name, "payload", true);
	auto s4 = FAState<std::string, std::string>("s4", rule_name, "payload", true);
	auto s5 = FAState<std::string, std::string>("s5", rule_name, "payload", true);

	std::vector<FAState<std::string, std::string>> from_state_1 {init_state};
	std::vector<FAState<std::string, std::string>> from_state_2 {init_state, s4};

	// WHEN
	NFA<std::string> subject = NFA<std::string>(init_state);
	subject.addTransition(init_state, s2, "a");
	subject.addTransition(s2, s3, "b");
	subject.addTransition(init_state, s4, "c");
	subject.addTransition(s4, s5, "a");

	// THEN    
	EXPECT_EQ((subject.move(from_state_1, "a"))->size(), 1); 
	EXPECT_EQ((*(subject.move(from_state_1, "a")))[0].getId(), "s2"); 
	EXPECT_EQ((subject.move(from_state_1, "c"))->size(), 1);
	EXPECT_EQ((*(subject.move(from_state_1, "c")))[0].getId(), "s4"); 
	
	EXPECT_EQ((subject.move(from_state_2, "a"))->size(), 2);
	EXPECT_EQ((*(subject.move(from_state_2, "a")))[0].getId(), "s2"); 
	EXPECT_EQ((*(subject.move(from_state_2, "a")))[1].getId(), "s5"); 
}

