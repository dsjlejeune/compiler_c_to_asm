/*
 * FILE GENERATED AUTOMATICALLY BY THE LEXER GENERATOR
 * DON'T MODIFY!
 * ALL MANUAL CHANGES ARE AT RISK TO BE OVERWRITTEN!
*/
#ifndef LEXER_LEXER_HPP_
#define LEXER_LEXER_HPP_

#include<filesystem>
#include "lexer/lexer_base.hpp"
#include "finite_automaton/nfa.hpp"

class Lexer: public LexerBase {
    public:
        Lexer(const fs::path& filename);

    protected:
        void buildNFA();
};

#endif // LEXER_LEXER_HPP_