#ifndef LEXER_LEXER_BASE_HPP_
#define LEXER_LEXER_BASE_HPP_

#include<filesystem>
#include<fstream>
#include<string>
#include<vector>

#include "finite_automaton/nfa.hpp"

namespace fs = std::filesystem;

typedef struct {
	std::string token_id; // TODO: change it to int (along with adding a symbol table/ hashmap to the rule name) for limiting the memory footprint
	std::string lexeme;
} token_t;

class LexerBase {
	public:
		LexerBase(const fs::path& filename);
		~LexerBase();
		
		const token_t* nextToken();
		const token_t* nextNonEmptyToken();

	protected:
		std::string source_code;

		unsigned int stream_position;
		unsigned int lookahead;

		NFA<std::string>* nfa = nullptr;

		int getIndexOfFirstAcceptingState(const std::vector<FAState<std::string, std::string>>& states);
		const std::vector<FAState<std::string, std::string>>* getAllAcceptingStates(const std::vector<FAState<std::string, std::string>>& states);
		bool hasSameRule(const std::vector<FAState<std::string, std::string>>& states);
		void buildNFA();
};

#endif
