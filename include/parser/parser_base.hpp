#ifndef PARSER_PARSER_BASE_HPP_
#define PARSER_PARSER_BASE_HPP_

#include<string>
#include<map>
#include<stack>
#include<utility>

#include "lexer/lexer.hpp"
#include "parser/parser_actions.hpp"

class ParserBase {
	public:
		ParserBase(const Lexer& lexer): lexer(lexer) {};
		~ParserBase() {};

	protected:
		const Lexer& lexer;
		const token_t* next_token;
		// TODO: State (stack & next_token)

		// Parsing table
		std::map<std::pair<int, std::string>, SLRAction*> table_action;
		std::map<std::pair<int, std::string>, int> table_goto;

		// Stack keeping the current state of the parser
		std::stack<int> working_stack;

		void init_stack(int start_state) { this->working_stack.push(start_state); }
		
};

#endif // PARSER_PARSER_BASE_HPP_
