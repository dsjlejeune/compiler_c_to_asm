#ifndef PARSER_PARSER_ACTIONS_HPP_
#define PARSER_PARSER_ACTIONS_HPP_

#include<string>
#include<stack>

class SLRAction {
	
	public:
	virtual bool apply(std::stack<int>* stack_state, std::string input_terminal_name) = 0;
};

class SLRShift: public SLRAction {
	protected:
	int state;

	public:
	SLRShift(int state): state(state) {};
	//~SLRShift() {};

	bool apply(std::stack<int>* stack_state, std::string input_terminal_name) override;

};

class SLRReduce: public SLRAction {
	protected:
	std::string rule_name;
	int idx_disjunction;
	int nbr_symbols_to_pop; 

	public:
	SLRReduce(std::string rule_name, int idx_disjunction, int nbr_symbols_to_pop): rule_name(rule_name), idx_disjunction(idx_disjunction), nbr_symbols_to_pop(nbr_symbols_to_pop) {};
	//~SLRReduce() {};

	bool apply(std::stack<int>* stack_state, std::string input_terminal_name) override;
};

class SLRAccept: public SLRAction {
	public:
	SLRAccept() {};
	//~SLRAccept() {};

	bool apply(std::stack<int>* stack_state, std::string input_terminal_name) override;
};

#endif // PARSER_PARSER_ACTIONS_HPP_
