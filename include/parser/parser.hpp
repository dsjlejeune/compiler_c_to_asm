/*
 * FILE GENERATED AUTOMATICALLY BY THE PARSER GENERATOR
 * DON'T MODIFY!
 * ALL MANUAL CHANGES ARE AT RISK TO BE OVERWRITTEN!
*/
#ifndef PARSER_PARSER_HPP_
#define PARSER_PARSER_HPP_

#include "lexer/lexer.hpp"
#include "parser/parser_base.hpp"

class Parser: public ParserBase {
    public:
        Parser(const Lexer& lexer);
        ~Parser() {};

    protected:
        void buildParsingTable();
};

#endif // PARSER_PARSER_HPP_

