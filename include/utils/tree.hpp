#ifndef UTILS_TREE_HPP_
#define UTILS_TREE_HPP_

#include<iostream>
#include<functional>

#include "node.hpp"

template<class T> class Tree {
   public:
	Tree(): root(nullptr) {};
	Tree(const T& root_payload);
	virtual ~Tree();
	
	Node<T>* add_node(Node<T> const*  new_node_parent, const T& payload);
	Node<T>* add_child_with_payload(Node<T>* parent, const T& payload);
	
	// Setter
	Node<T>* set_root(const T& root_payload);

	// Getter
	Node<T>* get_root();


   protected:
	Node<T>* root;

	void bfs_op(Node<T>** init_node, std::function<void(Node<T>**)> op); 
	
	virtual void free_memory();

	virtual void free_node(Node<T>** node);
};


template<class T> Tree<T>::Tree(const T& root_payload): root(nullptr) {
	this->set_root(root_payload);
}

template<class T> Tree<T>::~Tree() {
	this->free_memory();
}

template<class T> Node<T>* Tree<T>::add_node(Node<T> const* new_node_parent, const T& payload) {
	auto new_node = new Node(payload);
	new_node->add_parent(new_node_parent);

	new_node_parent->add_child(new_node);
	
	return new_node;
}

template<class T> Node<T>* Tree<T>::add_child_with_payload(Node<T>* new_node_parent, const T& payload) {
	auto new_node = new Node(payload);
	new_node->add_parent(new_node_parent);

	new_node_parent->add_child(new_node);
	
	return new_node;
}


// Setter
template<class T> Node<T>* Tree<T>::set_root(const T& root_payload) {
	if (this->root == nullptr) {
		this->root = new Node(root_payload);
		return this->root;
	} else {
		// TODO: Throw an Exception here
	}
}

// Getter

template<class T> Node<T>* Tree<T>::get_root() {
	return this->root;
}

// Protected

template<class T> void Tree<T>::bfs_op(Node<T>** init_node, std::function<void(Node<T>**)> op) {
	if (*init_node != nullptr && (*init_node)->get_children().size() == 0) {
		op(init_node);
	} else {
		for(auto elem : (*init_node)->get_children()) {
			this->bfs_op(&elem, op);
		}

		op(init_node);
	}

}

/*
 * Cleanup the memory in bread first search fashion
 */
template<class T> void Tree<T>::free_memory() {
	this->bfs_op(&this->root,
				 std::bind(&Tree<T>::free_node, this, std::placeholders::_1)
				);
}

template<class T> void Tree<T>::free_node(Node<T>** node) {	
	if( (*node)->get_parent() != nullptr) {
		auto siblings = (*node)->get_parent()->get_children();
		siblings.erase(siblings.begin());
	}

	delete *node;
	*node = nullptr;
}


// Deduction Guides

Tree(const char*) -> Tree<std::string>;

#endif // UTILS_TREE_HPP_
