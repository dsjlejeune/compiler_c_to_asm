#ifndef UTILS_FILE_READER_HPP_
#define UTILS_FILE_READER_HPP_

#include<filesystem>
#include<fstream>
#include<iostream>
#include<string>
#include<exception>
#include<sstream>

namespace fs = std::filesystem;

class FileDoesNotExistException: public std::exception {
	public:
	 explicit FileDoesNotExistException(const fs::path& file) : path_(file) {}
	 virtual const char* what() const throw() {
		//std::stringstream ss;
		//ss << "File " << this->path_ << " does not exist";
	 	//return ss.str().c_str();
		return "File not found";
	 }
	protected:
		const fs::path path_;
};

/***
 * Simple File reader: putting all the source file in memory.
 * Suboptimal but good enough as a first step
 */
class FileReader: public std::iterator<std::input_iterator_tag, int> {
	public:
		FileReader(const fs::path&);
		~FileReader();

		std::string read();
		void close();

	private:
		const fs::path& path;
		std::ifstream stream;		
};
#endif
