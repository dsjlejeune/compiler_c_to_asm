#ifndef UTILS_NODE_HPP_
#define UTILS_NODE_HPP_

#include<vector>
#include<string>

template<class T> class Node {
   private:
	T payload;
	Node<T>* parent;
	std::vector<Node<T>*> children;

   public:
	Node(const T& payload);

   	// Setter
	void add_child(Node<T>* child);
	void add_parent(Node<T>* parent);

   	// Getter
   	T get_payload();
	Node<T>* get_parent();
   	std::vector<Node<T>*>& get_children();
   	Node<T>* get_child_at(unsigned int);
   			
};

template<class T> Node<T>::Node(const T& payload) {
	this->payload = payload;
	this->parent = nullptr;
}

// Setters

template<class T> void Node<T>::add_child(Node<T>* child) {
	this->children.push_back(child);
    child->add_parent(this); 
}

template<class T> void Node<T>::add_parent(Node<T>* parent) {
	this->parent = parent;
}

// Getters

template<class T> Node<T>* Node<T>::get_parent() {
	return this->parent;
}

template<class T> T Node<T>::get_payload() {
	return this->payload;
}

template<class T> std::vector<Node<T>*>& Node<T>::get_children() {
	return this->children;	
}

template<class T> Node<T>* Node<T>::get_child_at(unsigned int i) {
	return this->children[i];
}

// Deduction Guides

Node(const char*) -> Node<std::string>;

#endif // UTILS_NODE_HPP_
