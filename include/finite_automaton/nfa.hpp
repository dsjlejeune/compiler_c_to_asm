#ifndef FA_NFA_HPP_
#define FA_NFA_HPP_

#include<string>
#include<map>
#include<vector>
#include<functional>
#include<stack>

#include "finite_automaton/fastate.hpp"

#define EPSILON ""

template<class T>
class NFA {
	protected:
	FAState<T, std::string> init_state;   
	std::map<std::pair<FAState<T, std::string>, std::string>, std::vector<FAState<T, std::string>>*> transition_matrix;


	public:
	NFA(FAState<T, std::string>& init_state): init_state(init_state) {};
	~NFA();

	const FAState<T, std::string>& getInitState();		

	void addTransition(FAState<T, std::string>& from_state, FAState<T, std::string>& to_state, const std::string on_char);
	const std::vector<FAState<T, std::string>>* epsilon_closure(const std::vector<FAState<T, std::string>>& from_state);
	const std::vector<FAState<T, std::string>>* move(const std::vector<FAState<T, std::string>>& current_states, std::string on_char);
};

template <class X> struct std::hash<std::pair<FAState<X, std::string>, std::string>> {
	size_t operator()(const std::pair<FAState<X, std::string>, std::string>& elem) const {
		std::hash<X> id_hasher;
		return id_hasher(elem.first.getId()) ^ string_hasher(elem.second); // TODO: update the hash function, ok for now but bad for collisions
	}
};


template<class T>
NFA<T>::~NFA() {
	for (auto& [key, value]: this->transition_matrix) {
		//this->transition_matrix.erase(key);
		delete value;
	} 
}

template<class T>
const FAState<T, std::string>& NFA<T>::getInitState() {
	return this->init_state;
}

template<class T>
void NFA<T>::addTransition(FAState<T, std::string>& from_state, FAState<T, std::string>& to_state, const std::string on_char) {
	std::vector<FAState<T, std::string>>* value;
	auto key = std::pair<FAState<T, std::string>, std::string>(from_state, on_char);

	if(this->transition_matrix.find(key) != this->transition_matrix.end()) {
		value = this->transition_matrix[key];
		value->push_back(to_state);
	} else {
		value = new std::vector<FAState<T, std::string>>();
		value->push_back(to_state);
		this->transition_matrix.emplace(key, value);
	}
}

template<class T>
const std::vector<FAState<T, std::string>>* NFA<T>::epsilon_closure(const std::vector<FAState<T, std::string>>& from_states) {
	std::stack<FAState<T, std::string>> bfs_stack;
	std::map<FAState<T, std::string>, bool> states_in_epsilon_closure; // needed for O(1) query.The memory trade-off of having this map is fine.
	std::vector<FAState<T, std::string>>* closure = new std::vector<FAState<T, std::string>>();

	for(FAState<T, std::string> elem: from_states) {
		states_in_epsilon_closure[elem] = true;
		closure->push_back(elem);

		if(this->transition_matrix.find(std::pair<FAState<T, std::string>, std::string>(elem, EPSILON)) != this->transition_matrix.end()) {
			bfs_stack.push(elem);
		}
	}

	while(!bfs_stack.empty()) {
		FAState<T, std::string> top_elem = bfs_stack.top();
		bfs_stack.pop();
		auto key = std::pair<FAState<T, std::string>, std::string>(top_elem, EPSILON);

		if(this->transition_matrix.find(key) != this->transition_matrix.end()) {
			states_in_epsilon_closure[top_elem] = true;

			for(FAState<T,std::string> next_state: *(this->transition_matrix[key])) {
				bfs_stack.push(next_state);
				closure->push_back(next_state);
			}
			
		}
	}
	
	if (closure->size() == 0) {
		delete closure;
		return nullptr;
	} else {
		return closure;		
	}
}

template<class T>
const std::vector<FAState<T, std::string>>* NFA<T>::move(const std::vector<FAState<T, std::string>>& current_states, const std::string on_char) {
	std::vector<FAState<T, std::string>>* ret = new std::vector<FAState<T, std::string>>();

	for(const auto& from_state: current_states) {
		auto key = std::pair<FAState<T, std::string>, std::string>(from_state, on_char);
		auto any_char_key = std::pair<FAState<T, std::string>, std::string>(from_state, "ANY");

		if(this->transition_matrix.find(key) != this->transition_matrix.end()) {
			for(const auto& elem: *(this->transition_matrix[key])) {
				ret->push_back(elem);
			}
		} else if(this->transition_matrix.find(any_char_key) != this->transition_matrix.end()) 
			for(const auto& elem: *(this->transition_matrix[any_char_key])) {
				ret->push_back(elem);
		}
	}

	return ret;
}

#endif // FA_NFA_HPP_
