#ifndef _FA_STATE_HPP_
#define _FA_STATE_HPP_

#include<string>

template<class T, class U> class FAState {
	private:
	T id;
	U payload;
	std::string rule_name;
	bool is_accepting_state = false;
	

	public:
	FAState(const T& id, const std::string& rule_name): id(id), rule_name(rule_name) {};
	FAState(const T& id, const std::string& rule_name, const U& payload): id(id), rule_name(rule_name), payload(payload) {};
	FAState(const T& id, const std::string& rule_name, const U& payload, bool accepting): id(id), rule_name(rule_name), payload(payload), is_accepting_state(accepting) {};

	// Setter
	void setAccepting(bool accepting);
	
	// Getter
	const T& getId() const;
	const std::string& getRuleName() const;
	const U& getPayload() const;
	bool isAccepting() const;

	// Operator overloading
	template<class X, class Y> friend bool operator<(const FAState<X,Y>& lhs, const FAState<X,Y>& rhs) { return lhs.getId() < rhs.getId(); };
};

// Setter
template<class T, class U> void FAState<T,U>::setAccepting(bool accepting_state) {
	this->is_accepting_state = accepting_state; 
}

// Getter
template<class T, class U> const T& FAState<T,U>::getId() const {
	return this->id;
}

template<class T, class U> const std::string& FAState<T,U>::getRuleName() const {
	return this->rule_name;
}

template<class T, class U> const U& FAState<T,U>::getPayload() const {
	return this->payload;
}

template<class T, class U> bool FAState<T,U>::isAccepting() const {
	return this->is_accepting_state;
}

#endif // _FA_STATE_HPP_
